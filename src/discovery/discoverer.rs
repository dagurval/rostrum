use crate::{
    def::PROTOCOL_VERSION_MIN,
    discovery::peer::{connect_to_electrum_server, electrum_request},
    metrics::Metrics,
    signal::Waiter,
};
use anyhow::Result;
use bitcoincash::Network;
use rand::Rng;
use rayon::prelude::IntoParallelRefIterator;
use serde_json::Value;
use std::{
    collections::{HashMap, HashSet},
    convert::TryInto,
    io::{BufRead, BufReader, Write},
    net::IpAddr,
    sync::{atomic::AtomicBool, Arc, Mutex},
    time::{Duration, Instant},
};

use crate::config::Config;
use crate::discovery::{
    peer::{CandidatePeer, GoodKnownPeer, ServerPeer},
    seed::{SEED_PEERS_MAINNET, SEED_PEERS_TESTNET, SEED_PEERS_TESTNET4},
};
use rayon::prelude::*;

// We only accept limited number of peers per subnet as sybil attack mitigation
const MAX_PER_OCTET_PREFIX: usize = 5;
// Max number of candidates in queue for consideration
const MAX_CANDIDATES: usize = 100;
// If candidate queue is full, accept a new one only if this many seconds have passed
const CANDIDATE_EVICT_DURATION: Duration = Duration::from_secs(120);
// How long until we re-visit a good peer to reconsider if it's good or not.
const GOOD_PEER_REVISIT_DURATION: Duration = Duration::from_secs(900);
// How many servers can be queued for our announcement
const ANNOUNCE_QUEUE_MAX_SIZE: usize = 200;

// Get IP-prefix
fn get_prefix(addr: &IpAddr) -> [u8; 2] {
    match addr {
        IpAddr::V4(ipv4) => ipv4.octets()[..2].try_into().unwrap(),
        IpAddr::V6(ipv6) => ipv6.octets()[..2].try_into().unwrap(),
    }
}

/**
 * Maintains a list of good server peers and considers new ones.
 */
pub struct PeerDiscoverer {
    good: Mutex<HashMap<u64, GoodKnownPeer>>,
    /// List of peers that have submitted themselves to us for consideration.
    candidates: Mutex<Vec<CandidatePeer>>,
    /// Last time we added someone to the candidate list.
    last_add: Mutex<Instant>,
    /// Thread responsible for visiting server peers
    discovery_thread: Mutex<Option<std::thread::JoinHandle<()>>>,
    discovery_thread_kill: Arc<AtomicBool>,
    /// Yet to announce to
    announce_queue: Mutex<Vec<CandidatePeer>>,
    /// Peers we have (attempted to) announce to.
    announce_done: Mutex<HashSet</* peer id */ u64>>,

    /// How many good nodes we currently know of.
    metric_good_now: prometheus::IntGauge,
    /// How many good nodes we have added in total.
    metric_good_added: prometheus::IntCounter,
    /// How many good nodes we removed total.
    metric_good_removed: prometheus::IntCounter,
    /// How many candidates we currently have.
    metric_candidates_now: prometheus::IntGauge,
    /// How many candidates we have added in total.
    metric_candidates_added: prometheus::IntCounter,
    /// How many candidates we have rejected.
    metric_candidates_rejected: prometheus::IntCounter,
    /// How many peers are queued for announcement.
    metric_announce_now: prometheus::IntGauge,
}

// TODO: Store good peers in a database
impl PeerDiscoverer {
    pub fn new(config: &Config, metrics: &Metrics) -> Arc<Self> {
        let d = Arc::new(Self {
            good: Mutex::new(HashMap::default()),
            candidates: Mutex::new(match config.network_type {
                Network::Bitcoin => SEED_PEERS_MAINNET.to_vec(),
                Network::Testnet => SEED_PEERS_TESTNET.to_vec(),
                Network::Testnet4 => SEED_PEERS_TESTNET4.to_vec(),
                Network::Regtest | Network::Scalenet | Network::Chipnet => Vec::default(),
            }),
            last_add: Mutex::new(Instant::now()),
            discovery_thread: Mutex::new(None),
            discovery_thread_kill: Arc::new(AtomicBool::new(false)),
            announce_done: Mutex::new(HashSet::default()),
            announce_queue: Mutex::new(Vec::with_capacity(ANNOUNCE_QUEUE_MAX_SIZE)),

            metric_good_now: metrics.gauge_int(prometheus::Opts::new(
                "rostrum_discover_good",
                "# of known good server peers",
            )),
            metric_good_added: metrics.counter_int(prometheus::Opts::new(
                "rostrum_discover_added",
                "# of known good server peers added (accumulative)",
            )),
            metric_good_removed: metrics.counter_int(prometheus::Opts::new(
                "rostrum_discover_removed",
                "# of known good server peers removed (accumulative)",
            )),
            metric_candidates_now: metrics.gauge_int(prometheus::Opts::new(
                "rostrum_discover_candidates",
                "# of known untested server peers",
            )),
            metric_candidates_added: metrics.counter_int(prometheus::Opts::new(
                "rostrum_discover_candidates_added",
                "# of server peer candidates added (accumulative)",
            )),
            metric_candidates_rejected: metrics.counter_int(prometheus::Opts::new(
                "rostrum_discover_candidates_rejected",
                "# of server peer candidates rejected (accumulative)",
            )),
            metric_announce_now: metrics.gauge_int(prometheus::Opts::new(
                "rostrum_discover_announce_queue",
                "# of server peers in queue for announcement",
            )),
        });

        let metric_tested = metrics.counter_int(prometheus::Opts::new(
            "rostrum_discover_candidates_tested",
            "# of server peer test connections made (accumulative)",
        ));

        let thread_d = Arc::clone(&d);
        if config.announce {
            info!("Peer discovery thread started.");
            *d.discovery_thread.lock().unwrap() =
                Some(crate::thread::spawn("discovery", move || {
                    loop {
                        if Waiter::wait_or_shutdown(Duration::from_secs(10)).is_err()
                            || thread_d
                                .discovery_thread_kill
                                .load(std::sync::atomic::Ordering::Relaxed)
                        {
                            // Exit thread
                            return Ok(());
                        }

                        // Checking if good nodes are still good is takes precedence over finding a new candidate.
                        let needs_revisit = thread_d
                            .good
                            .lock()
                            .unwrap()
                            .iter()
                            .filter_map(|(_, p)| {
                                if p.last_visit.elapsed() >= GOOD_PEER_REVISIT_DURATION {
                                    // Time to re-visit this peer server
                                    Some(p.clone())
                                } else {
                                    None
                                }
                            })
                            .take(1)
                            .collect::<Vec<GoodKnownPeer>>();

                        if let Some(peer) = needs_revisit.first() {
                            metric_tested.inc();
                            if test_if_good_peer(peer) {
                                thread_d.bump_last_visit(&peer.id())
                            } else {
                                thread_d.remove_good(&peer.id())
                            }
                            continue;
                        }

                        // Good nodes are up-to-date. Check a random candidate.
                        let candidate = {
                            let lock = thread_d.candidates.lock().unwrap();
                            if lock.is_empty() {
                                None
                            } else {
                                // We take a copy so that it isn't re-added while we're considering it
                                let index = rand::thread_rng().gen_range(0..lock.len());
                                lock.get(index).cloned()
                            }
                        };

                        if let Some(peer) = candidate {
                            if test_if_good_peer(&peer) {
                                thread_d.promote_candidate(&peer.id());
                                thread_d.maybe_add_announce_queue(peer);
                            } else {
                                thread_d.reject_candidate(&peer.id());
                            }
                        }
                    }
                }));
        } else {
            info!("Peer discovery thread disabled.")
        }

        d
    }

    /// Update the 'last visit' attribute of a good node
    pub fn bump_last_visit(&self, id: &u64) {
        match self.good.lock().unwrap().get_mut(id) {
            Some(p) => p.last_visit = Instant::now(),
            None => {
                debug_assert!(false);
                warn!("Failed to bump good peer {id}");
            }
        };
    }

    /// Promote a candidate peer into good node
    pub fn promote_candidate(&self, id: &u64) {
        let mut lock = self.candidates.lock().unwrap();
        let candidate = lock.iter().enumerate().find(|(_, p)| &p.id() == id);
        match candidate {
            Some((index, _)) => {
                let c = lock.remove(index);
                drop(lock);
                self.good.lock().unwrap().insert(c.id(), c.into());
                self.metric_good_added.inc();
                self.metric_good_now.inc();
            }
            None => {
                debug_assert!(false);
                warn!("Tried to promote a candidate that does not exist")
            }
        }
    }

    /// Reject a candidate
    pub fn reject_candidate(&self, id: &u64) {
        let mut lock = self.candidates.lock().unwrap();
        let candidate = lock.iter().enumerate().find(|(_, p)| &p.id() == id);
        match candidate {
            Some((index, _)) => {
                lock.remove(index);
                self.metric_candidates_rejected.inc();
                self.metric_candidates_now.set(lock.len() as i64);
            }
            None => {
                debug_assert!(false);
                warn!("Tried to reject a candidate that does not exist")
            }
        }
    }

    /// Remove a node no longer considered good
    pub fn remove_good(&self, id: &u64) {
        self.good.lock().unwrap().remove(id);
        self.metric_good_now.dec();
        self.metric_good_removed.inc();

        // If it went offline, allow us to announce ourselves to it if we see it again.
        self.announce_done.lock().unwrap().remove(id);
    }

    /// How many good peers we have with given prefix.
    fn len_good_peers_with_prefix(&self, prefix: &[u8; 2]) -> usize {
        self.good
            .lock()
            .unwrap()
            .par_iter()
            .filter(|(_, p)| &get_prefix(&p.ip()) == prefix)
            .count()
    }

    /// If we have a good peer with given ID
    fn has_good(&self, id: u64) -> bool {
        self.good.lock().unwrap().iter().any(|(_, p)| p.id() == id)
    }

    /// Add a new peer for discoverer to consider
    pub fn add_candidate(&self, candidate: CandidatePeer) -> Result<()> {
        if self.len_good_peers_with_prefix(&get_prefix(&candidate.ip())) >= MAX_PER_OCTET_PREFIX {
            bail!("Have enough good servers in this subnet");
        }

        let candidate_id = candidate.id();

        if self.has_good(candidate_id) {
            return Ok(());
        }

        let mut c = self.candidates.lock().unwrap();
        if c.iter().any(|existing| existing.id() == candidate_id) {
            return Ok(());
        }

        if c.len() < MAX_CANDIDATES {
            c.push(candidate);
            *self.last_add.lock().unwrap() = Instant::now();
            self.metric_candidates_added.inc();
            self.metric_candidates_now.inc();
            return Ok(());
        }

        // Our candidate list is full, but if it's been a long time since we
        // accepted a candidate, just randomly drop some other and accept this one.
        if self.last_add.lock().unwrap().elapsed() >= CANDIDATE_EVICT_DURATION {
            trace!("Evicting random and adding candidate {:?}", candidate);
            let index = rand::thread_rng().gen_range(0..c.len());
            c.remove(index);
            c.push(candidate);
            *self.last_add.lock().unwrap() = Instant::now();
            self.metric_candidates_added.inc();
            return Ok(());
        }
        bail!("Too many peers for consideration. Try again later.");
    }

    // Get list of good peers.
    pub fn get_peers(&self) -> Vec<GoodKnownPeer> {
        self.good
            .lock()
            .unwrap()
            .iter()
            .map(|(_, peers)| peers)
            .cloned()
            .collect()
    }

    /// Take a peer from the announce queue.
    pub fn next_announce(&self) -> Option<CandidatePeer> {
        let mut lock = self.announce_queue.lock().unwrap();

        if lock.is_empty() {
            return None;
        }
        // take a random peer to mitigate sybil
        let index = rand::thread_rng().gen_range(0..lock.len());
        let peer = lock.remove(index);

        self.announce_done.lock().unwrap().insert(peer.id());
        self.metric_announce_now.set(lock.len() as i64);
        Some(peer)
    }

    /// Add peer to queue for peers to announce this server to (if applicable).
    pub fn maybe_add_announce_queue(&self, peer: CandidatePeer) {
        if self.announce_done.lock().unwrap().contains(&peer.id()) {
            return;
        }
        let mut lock = self.announce_queue.lock().unwrap();
        if lock.len() >= ANNOUNCE_QUEUE_MAX_SIZE {
            trace!("Announce queue full, ignoring {}", peer.ip().to_string());
            return;
        }
        lock.push(peer);
        self.metric_announce_now.set(lock.len() as i64);
    }

    /// Take a copy of a random good peer
    pub fn random_good(&self) -> Option<GoodKnownPeer> {
        let lock = self.good.lock().unwrap();
        if lock.is_empty() {
            return None;
        }
        let index = rand::thread_rng().gen_range(0..lock.len());
        lock.iter()
            .skip(index)
            .take(1)
            .map(|(_, p)| p)
            .collect::<Vec<_>>()
            .get(0)
            .cloned()
            .cloned()
    }
}

impl Drop for PeerDiscoverer {
    fn drop(&mut self) {
        self.discovery_thread_kill
            .store(true, std::sync::atomic::Ordering::Relaxed);
    }
}

/**
 * Connects to a server and attempts to determine if it's a well behaved server.
 * Well behaved means that at minimum it's serving the same network.
 */
pub fn test_if_good_peer(peer: &dyn ServerPeer) -> bool {
    trace!(
        "Considering server peer {} (IP: {})",
        peer.hostname(),
        peer.ip()
    );

    let mut stream = match connect_to_electrum_server(peer) {
        Ok(s) => s,
        Err(e) => {
            trace!("Peer considered bad due to error: {e}");
            return false;
        }
    };

    // Start with a version call
    let req = electrum_request(
        1,
        "server.version",
        json!(vec!["Rostrum", PROTOCOL_VERSION_MIN]),
    );
    let mut reader = BufReader::new(stream.try_clone().unwrap());
    if let Err(e) = stream.write(&req) {
        trace!("Failed write request: {e}");
        return false;
    }
    if let Err(e) = stream.flush() {
        trace!("Stream write flush failed: {e}");
        return false;
    }

    let mut response: String = String::new();
    if let Err(e) = reader.read_line(&mut response) {
        trace!("Failed to read server version response: {e}");
        return false;
    }

    let response: Value = match serde_json::from_str(&response) {
        Ok(r) => r,
        Err(e) => {
            trace!("Failed to parse server version response: {e}");
            return false;
        }
    };

    let _result: Value = match response.get("result") {
        Some(_) => {
            // TODO: Consider protocol version
            Value::Null
        }
        None => {
            trace!("No result from server version request");
            return false;
        }
    };

    // TODO: Check block height is similar to ours.
    true
}
