use std::{
    collections::HashMap,
    io::{BufRead, BufReader, Write},
    sync::{atomic::AtomicBool, Arc},
    time::Duration,
};

use bitcoincash::Network;
use configure_me::toml::from_str;
use serde_json::Value;

use crate::{
    config::{default_electrum_port, Config},
    discovery::peer::electrum_request,
    metrics::Metrics,
};

use crate::{
    chaindef::BlockHash,
    def::{PROTOCOL_HASH_FUNCTION, PROTOCOL_VERSION_MAX, PROTOCOL_VERSION_MIN},
    discovery::peer::connect_to_electrum_server,
    signal::Waiter,
};

use super::{
    discoverer::PeerDiscoverer,
    peer::{CandidatePeer, ServerPeer},
};
use crate::def::ROSTRUM_VERSION;
use anyhow::{Context, Result};
use std::net::IpAddr;

/**
 * Max number of candidates to consider from a 'server.peers.subscribe' response.
 */
const MAX_CANDIDATES_PER_REQ: usize = 50;

#[derive(Debug, Clone, Serialize)]
pub struct ServerFeatures {
    genesis_hash: BlockHash,
    hash_function: &'static str,
    protocol_max: &'static str,
    protocol_min: &'static str,
    server_version: String,
    hosts: Option<HashMap<String, HashMap<String, Option<u16>>>>,
    pruning: Option<u64>,
    tokens: bool,
    spent_utxo: bool,
}

pub struct PeerAnnouncer {
    features: ServerFeatures,
    /// Thread responsible for announcing us to other server peers
    announce_thread: Option<std::thread::JoinHandle<()>>,
    announce_thread_kill: Arc<AtomicBool>,
}

impl PeerAnnouncer {
    pub fn new(
        genesis_hash: BlockHash,
        discoverer: Arc<PeerDiscoverer>,
        config: &Config,
        metrics: &Metrics,
    ) -> Self {
        let mut enabled = config.announce;
        let mut hostname = config.announce_hostname.clone();

        if enabled && hostname.is_none() {
            // Try to find our public IP and use as hostname.
            // TODO: Use other electrum servers to find IP, rather than external service.
            debug!("Auto-deteching public IP");
            let agent: ureq::Agent = ureq::AgentBuilder::new()
                .timeout_read(Duration::from_secs(5))
                .timeout_write(Duration::from_secs(5))
                .build();
            let response = agent.get("https://api.ipify.org").call();
            let body = match response {
                Ok(response) => response.into_string().unwrap_or_default(),
                Err(e) => {
                    debug!("Failed to request public IP: {e}.");
                    String::default()
                }
            };
            let ip: Option<IpAddr> = body.parse().ok();
            if let Some(ip) = ip {
                debug!("Detected public as {}", ip.to_string());
                hostname = Some(ip.to_string());
            }
        }

        if enabled && hostname.is_none() {
            info!("Peer announcer: Argument announce-hostname is not set and unable to auto-detect. Peer announcement disabled.");
            enabled = false;
        }

        let hosts: Option<HashMap<String, HashMap<String, Option<u16>>>> = if enabled {
            let hostname = hostname.unwrap();
            let ports: HashMap<String, Option<u16>> = [
                (
                    "tcp_port".to_string(),
                    Some(config.electrum_rpc_addr.port()),
                ),
                ("ssl_port".to_string(), config.announce_ssl_port),
                ("ws_port".to_string(), Some(config.electrum_ws_addr.port())),
                ("wss_port".to_string(), config.announce_wss_port),
            ]
            .iter()
            .cloned()
            .collect();
            Some([(hostname, ports)].iter().cloned().collect())
        } else {
            None
        };

        let features = ServerFeatures {
            genesis_hash,
            hash_function: PROTOCOL_HASH_FUNCTION,
            protocol_min: PROTOCOL_VERSION_MIN,
            protocol_max: PROTOCOL_VERSION_MAX,
            server_version: format!("Rostrum {ROSTRUM_VERSION}"),
            hosts,
            pruning: None,
            tokens: true,
            spent_utxo: true,
        };

        let thread_kill = Arc::new(AtomicBool::new(false));

        let mut announcer = Self {
            features: features.clone(),
            announce_thread: None,
            announce_thread_kill: Arc::clone(&thread_kill),
        };

        if enabled {
            let announce_requests = metrics.counter_int(prometheus::Opts::new(
                "rostrum_discover_announce_request",
                "How often we announced ourselfs (attempts) to other server peers",
            ));
            let subscribe_requests = metrics.counter_int(prometheus::Opts::new(
                "rostrum_discover_peer_subscribe_request",
                "How often we requested (attempts) good peers from other server peers",
            ));

            let network = config.network_type;
            let announce_thread = crate::thread::spawn("announce", move || {
                loop {
                    if Waiter::wait_or_shutdown(Duration::from_secs(1)).is_err() {
                        return Ok(());
                    }
                    if thread_kill.load(std::sync::atomic::Ordering::Relaxed) {
                        return Ok(());
                    }
                    if let Some(peer) = discoverer.next_announce() {
                        announce_requests.inc();
                        announce_to_peer(&peer, &features);
                        continue;
                    }

                    // Chill, so we don't flood our good peers with requests
                    if Waiter::wait_or_shutdown(Duration::from_secs(120)).is_err() {
                        return Ok(());
                    }

                    // Out of peers to announce too. Check if a random good peer knows of new candidates.
                    if let Some(peer) = discoverer.random_good() {
                        subscribe_requests.inc();
                        let candidates = ask_for_candidates(&peer, &network);
                        for peer in candidates.into_iter().take(MAX_CANDIDATES_PER_REQ) {
                            discoverer.maybe_add_announce_queue(peer);
                        }
                    }
                }
            });
            announcer.announce_thread = Some(announce_thread);
        }

        announcer
    }

    pub fn server_features(&self) -> &ServerFeatures {
        &self.features
    }

    pub fn server_version(&self) -> &str {
        &self.features.server_version
    }
}

impl Drop for PeerAnnouncer {
    fn drop(&mut self) {
        self.announce_thread_kill
            .store(true, std::sync::atomic::Ordering::Relaxed);
    }
}

/**
 * Connects to a server and attempts to announce ourself.
 */
pub fn announce_to_peer(peer: &dyn ServerPeer, features: &ServerFeatures) {
    let mut stream = match connect_to_electrum_server(peer) {
        Ok(s) => s,
        Err(e) => {
            trace!("Failed to announce to peer: {e}");
            return;
        }
    };

    let mut reader = BufReader::new(stream.try_clone().unwrap());
    let req = electrum_request(1, "server.add_peer", json!(vec![features]));
    if let Err(e) = stream.write(&req) {
        trace!("Failed to request server.add_peer: {e}");
        return;
    }
    if let Err(e) = stream.flush() {
        trace!("Stream write flush failed: {e}");
        return;
    }

    let mut response: String = String::new();
    if let Err(e) = reader.read_line(&mut response) {
        trace!("Failed to read add peer response: {e}");
        return;
    }

    debug!("Add_peer response: {response}");
}

/**
 * Connects to a server and asks it for good candidates.
 */
pub fn ask_for_candidates(peer: &dyn ServerPeer, network: &Network) -> Vec<CandidatePeer> {
    let mut stream = match connect_to_electrum_server(peer) {
        Ok(s) => s,
        Err(e) => {
            trace!(
                "Failed to request peers from {}: {e}",
                peer.ip().to_string()
            );
            return Vec::default();
        }
    };

    let mut reader = BufReader::new(stream.try_clone().unwrap());
    let req = electrum_request(1, "server.peers.subscribe", json!(Vec::<Value>::default()));
    if let Err(e) = stream.write(&req) {
        trace!("Failed to request server.peers.subscribe: {e}");
        return Vec::default();
    }
    if let Err(e) = stream.flush() {
        trace!("Stream write flush failed: {e}");
        return Vec::default();
    }

    let mut response: String = String::new();
    if let Err(e) = reader.read_line(&mut response) {
        trace!("Failed to read peer subscribe response: {e}");
        return Vec::default();
    }
    trace!("<== {}", response);

    let response: Value = match serde_json::from_str(&response) {
        Ok(r) => r,
        Err(e) => {
            trace!("Failed to parse peer subscibe response: {e}");
            return Vec::default();
        }
    };

    match parse_candidate_response(response, network) {
        Ok(c) => c,
        Err(e) => {
            trace!("Invalid candidate response: ${e}");
            Vec::default()
        }
    }
}

fn parse_candidate_response(r: Value, network: &Network) -> Result<Vec<CandidatePeer>> {
    let result = r.get("result").context("No result")?;
    let peers = result
        .as_array()
        .context("expected array of peers, got something else")?;
    let mut candidates: Vec<CandidatePeer> = Default::default();
    for peer in peers.iter() {
        let ip = peer
            .get(0)
            .context("ip missing")?
            .as_str()
            .context("ip is not a string")?;
        let hostname = peer
            .get(1)
            .context("hostname missing")?
            .as_str()
            .context("hostname is not a string")?;
        let properties = peer
            .get(2)
            .context("peer properties missing")?
            .as_array()
            .context("expected peer properties to be an array")?;
        let ip: IpAddr = match ip.parse() {
            Ok(ip) => ip,
            Err(_) => {
                // Cannot parse IP. Might be onion. Ignore peer.
                continue;
            }
        };
        let mut candidate = CandidatePeer {
            tcp_port: None,
            ssl_port: None,
            hostname: Some(hostname.to_string()),
            ip,
        };
        for p in properties {
            let p = p.as_str().context("peer property was not a string")?;
            match p.chars().next() {
                Some('t') => {
                    candidate.tcp_port = match from_str(&p[1..]) {
                        Ok(port) => Some(port),
                        Err(_) => {
                            // It's OK not to provide a port number. In this case it's the default port.
                            default_electrum_port(network)
                        }
                    };
                }
                Some('s') => {
                    candidate.ssl_port = match from_str(&p[1..]) {
                        Ok(port) => Some(port),
                        Err(_) => {
                            // It's OK not to provide a port number. In this case it's the default port.
                            // Hack: Assume +1 is the SSL port for all networks
                            default_electrum_port(network).and_then(|p| p.checked_add(1))
                        }
                    };
                }
                _ => { /* ignore property */ }
            }
        }
        candidates.push(candidate);
    }

    Ok(candidates)
}
