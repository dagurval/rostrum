// Known good servers to seed the server discovery.

use super::peer::CandidatePeer;

// Known good seed peers
#[cfg(feature = "nexa")]
pub const SEED_PEERS_MAINNET: &[CandidatePeer] = &[
    // rostrum.nexa.ink
    CandidatePeer {
        tcp_port: Some(20001),
        ssl_port: Some(20002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(3, 13, 182, 215)),
    },
    // rostrum.nexa.org
    CandidatePeer {
        tcp_port: Some(20001),
        ssl_port: Some(20002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(178, 62, 35, 191)),
    },
];

#[cfg(feature = "nexa")]
pub const SEED_PEERS_TESTNET: &[CandidatePeer] = &[];

// Testnet4 is BCH only
#[cfg(feature = "nexa")]
pub const SEED_PEERS_TESTNET4: &[CandidatePeer] = &[];

// Known good seed peers
#[cfg(not(feature = "nexa"))]
pub const SEED_PEERS_MAINNET: &[CandidatePeer] = &[
    // bitcoincash.network
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(3, 13, 182, 215)),
    },
    // electrs.bitcoinunlimited.info
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(101, 36, 125, 103)),
    },
    // bch.loping.net
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(180, 183, 226, 37)),
    },
];

#[cfg(not(feature = "nexa"))]
pub const SEED_PEERS_TESTNET: &[CandidatePeer] = &[
    // testnet.bitcoincash.network
    CandidatePeer {
        tcp_port: Some(60001),
        ssl_port: Some(60002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(3, 13, 182, 215)),
    },
    // tbch.loping.net
    CandidatePeer {
        tcp_port: Some(60001),
        ssl_port: Some(60002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(180, 183, 226, 37)),
    },
];

#[cfg(not(feature = "nexa"))]
pub const SEED_PEERS_TESTNET4: &[CandidatePeer] = &[
    // testnet.bitcoincash.network
    CandidatePeer {
        tcp_port: Some(62001),
        ssl_port: Some(62002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(3, 13, 182, 215)),
    },
    // tbch4.loping.net
    CandidatePeer {
        tcp_port: Some(62001),
        ssl_port: Some(62002),
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(180, 183, 226, 37)),
    },
];
