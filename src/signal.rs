use bitcoincash::Txid;
use crossbeam_channel as channel;
use crossbeam_channel::RecvTimeoutError;
use std::collections::HashSet;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant};

use anyhow::Result;

use crate::chaindef::BlockHash;

static GLOBAL_SHUTDOWN_SIGNAL: AtomicBool = AtomicBool::new(false);

// Type of signals we listen for in the main loop of the program.
pub enum SignalNotification {
    NewTxs(HashSet<Txid>),
    NewBlock(BlockHash),
    Timeout,
}

pub struct Waiter {
    receiver: channel::Receiver<SignalNotification>,
}

/// Signals we receive via p2p connections
pub struct NetworkNotifier {
    pub txid_sender: channel::Sender<HashSet<Txid>>,
    txid_receiver: channel::Receiver<HashSet<Txid>>,

    pub block_sender: channel::Sender<BlockHash>,
    block_receiver: channel::Receiver<BlockHash>,
}

impl NetworkNotifier {
    pub fn new() -> NetworkNotifier {
        let (block_sender, block_receiver) = channel::bounded::<BlockHash>(100);
        let (txid_sender, txid_receiver) = channel::bounded::<HashSet<Txid>>(5);
        NetworkNotifier {
            txid_sender,
            txid_receiver,
            block_sender,
            block_receiver,
        }
    }
}

impl Default for NetworkNotifier {
    fn default() -> Self {
        Self::new()
    }
}

fn notify(network_notifier: Arc<NetworkNotifier>) -> channel::Receiver<SignalNotification> {
    use crate::thread::spawn;
    use channel::select;
    let (s, r) = channel::unbounded();

    spawn("network_signal", move || {
        loop {
            Waiter::shutdown_check()?;
            select! {
                recv(network_notifier.txid_receiver) -> result => {
                    match result {
                        Ok(txs) => {
                            // Best effort
                            let _ = s.try_send(SignalNotification::NewTxs(txs));
                        }
                        Err(e) => {
                            debug!("network signal error: {}", e)
                        }
                    }
                }
                recv(network_notifier.block_receiver) -> result => {
                    match result {
                        Ok(blockhash) => {
                            // Best effort
                            let _ = s.try_send(SignalNotification::NewBlock(blockhash));
                        }
                        Err(e) => {
                            debug!("network signal error: {}", e)
                        }
                    }
                }
            }
        }
    });
    r
}

impl Waiter {
    #[cfg(not(windows))]
    fn start_shutdown_listener() {
        let posix_signals = &[
            signal_hook::consts::SIGINT,
            signal_hook::consts::SIGTERM,
            signal_hook::consts::SIGUSR1, // allow external triggering (e.g. via bitcoind `blocknotify`)
        ];
        let mut signals = signal_hook::iterator::Signals::new(posix_signals)
            .expect("failed to register signal hook");

        crate::thread::spawn("posix_signal", move || {
            for signal in signals.forever() {
                if signal == signal_hook::consts::SIGUSR1 {
                    // Ignore. Used to trigger block update in earlier versions.
                    continue;
                }
                info!("Shutdown triggered via signal {}", signal);
                GLOBAL_SHUTDOWN_SIGNAL.store(true, Ordering::Relaxed);
            }
            Ok(())
        });
    }

    #[cfg(windows)]
    fn start_shutdown_listener() {
        let (tx, rx) = channel::bounded(1);
        ctrlc::set_handler(move || tx.send(()).expect("Could not send signal on channel."))
            .expect("Error setting Ctrl-C handler");

        crate::thread::spawn("win_signal", move || {
            rx.recv().expect("Could not receive from channel.");
            info!("Shutdown triggered via signal");
            GLOBAL_SHUTDOWN_SIGNAL.store(true, Ordering::Relaxed);
            Ok(())
        });
    }

    pub fn start(network_notifier: Arc<NetworkNotifier>) -> Waiter {
        let waiter = Waiter {
            receiver: notify(network_notifier),
        };
        Waiter::start_shutdown_listener();
        waiter
    }
    pub fn shutdown_check() -> Result<()> {
        if GLOBAL_SHUTDOWN_SIGNAL.load(Ordering::Relaxed) {
            bail!("Shutdown triggered");
        }
        Ok(())
    }
    pub fn wait(&self, duration: Duration) -> Result<SignalNotification> {
        Self::shutdown_check()?;
        match self.receiver.recv_timeout(duration) {
            Ok(sig) => Ok(sig),
            Err(RecvTimeoutError::Timeout) => Ok(SignalNotification::Timeout),
            Err(RecvTimeoutError::Disconnected) => bail!("signal hook channel disconnected"),
        }
    }

    pub fn wait_or_shutdown(duration: Duration) -> Result<()> {
        let start = Instant::now();
        while start.elapsed() < duration {
            Self::shutdown_check()?;
            std::thread::sleep(Duration::from_millis(100));
        }
        Ok(())
    }
}
