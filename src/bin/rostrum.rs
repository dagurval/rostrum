extern crate rostrum;

extern crate anyhow;
#[macro_use]
extern crate log;

use anyhow::Result;
use std::sync::Arc;

use rostrum::{
    app::App,
    cache::TransactionCache,
    chain::Chain,
    chaindef::Block,
    config::Config,
    daemon::Daemon,
    discovery::{announcer::PeerAnnouncer, discoverer::PeerDiscoverer},
    doslimit::{ConnectionLimits, GlobalLimits},
    index::Index,
    metrics::Metrics,
    query::Query,
    rpc::Rpc,
    signal::SignalNotification,
    signal::{NetworkNotifier, Waiter},
    store::{is_compatible_version, DBContents, DBStore},
};

/// Ensure index get flushed to disk on exit.
/// In case a thread holds ownership of Arc<DBStore>, the thread
/// may not release its reference count in time for drop to get called.
struct FlushRAII {
    store: Arc<DBStore>,
}

impl Drop for FlushRAII {
    fn drop(&mut self) {
        info!("Flushing indexes to disk...");
        if let Err(e) = self.store.flush() {
            warn!("Flushing indexes failed: {e}");
            return;
        }
        info!("Flushing indexes done.")
    }
}

fn run_server(config: Arc<Config>) -> Result<()> {
    let network_notifier = Arc::new(NetworkNotifier::new());
    let signal = Waiter::start(network_notifier.clone());
    let metrics = Arc::new(Metrics::new(config.monitoring_addr).expect("Failed to start metrics"));

    let daemon = Arc::new(Daemon::new(&config, network_notifier, &metrics)?);

    // Perform initial indexing.
    if config.reindex {
        info!("Configuration flag `reindex` enabled. Running a full reindex.");
        DBStore::destroy(&config.db_path);
    }

    let compatible = is_compatible_version(&config.db_path, &metrics);

    if !compatible {
        info!("Incompatible database. Running full reindex.");
        DBStore::destroy(&config.db_path);
    }
    let store = Arc::new(
        DBStore::open(
            DBContents::ConfirmedIndex,
            &config.db_path,
            &metrics,
            !compatible || config.reindex, // force new flag if not compatible
        )
        .unwrap(),
    );
    let _flush_raii = FlushRAII {
        store: Arc::clone(&store),
    };
    let genesis: Block = daemon.get_genesis().expect("Failed to get genesis block");
    let genesis_hash = genesis.block_hash();
    let index = Index::load(
        store,
        Chain::new(genesis),
        daemon.clone(),
        &metrics,
        &config,
    )?;

    let discoverer = PeerDiscoverer::new(&config, &metrics);
    let announcer = PeerAnnouncer::new(genesis_hash, Arc::clone(&discoverer), &config, &metrics);

    let app = App::new(index, daemon.clone(), &config, discoverer, announcer)?;
    let mempool = Arc::new({
        let mut mempool_path = config.db_path.clone();
        mempool_path.push("mempool");
        rostrum::mempool::Tracker::new(&mempool_path, &metrics)
    });

    let tx_cache = TransactionCache::new(config.tx_cache_size as u64, &metrics);
    let query = Query::new(
        app.clone(),
        &metrics,
        tx_cache,
        mempool.clone(),
        config.network_type,
    )?;
    let connection_limits = ConnectionLimits::new(
        config.rpc_timeout,
        config.scripthash_subscription_limit,
        config.scripthash_alias_bytes_limit,
    );
    let global_limits = Arc::new(GlobalLimits::new(
        config.rpc_max_connections,
        config.rpc_max_connections_shared_prefix,
        &metrics,
    ));

    let rpc_addr = config.electrum_rpc_addr;
    let ws_addr = config.electrum_ws_addr;
    rostrum::thread::spawn("ws", move || {
        rostrum::wstcp::start_ws_proxy(ws_addr, rpc_addr);
        Ok(())
    });

    app.first_update()?;

    // Electrum RPC server
    // We start electrum server AFTER first update, because many applications
    // just assume that the server index is up-to-date
    let server: Rpc = Rpc::start(
        config.clone(),
        query.clone(),
        metrics,
        connection_limits,
        global_limits,
    );

    loop {
        match signal.wait(config.wait_duration) {
            Ok(sig) => {
                match sig {
                    SignalNotification::NewTxs(tx_invs) => {
                        trace!("NewTx notification");
                        let txs_changed = mempool.update_from_p2p(tx_invs, query.tx());
                        server.notify_scripthash_subscriptions(&[], txs_changed);
                        continue;
                    }
                    SignalNotification::NewBlock(blockhash) => {
                        trace!("NewBlock notification");

                        if app.index().chain().contains(&blockhash) {
                            // we have already seen this block
                            continue;
                        }

                        // Block not seen. Proceed to pull full update.
                    }
                    SignalNotification::Timeout => {
                        daemon.p2p_keepalive();
                        // Proceed to pull full update
                    }
                }
            }
            Err(err) => {
                info!("Stopping server: {}", err);
                break;
            }
        }

        // New block, or full pull (via timeout). Do a full update.
        let txs_changed = query.update_mempool()?;
        let (headers_changed, new_tip) = app.update_blocks()?;

        server.notify_scripthash_subscriptions(&headers_changed, txs_changed);
        if let Some(header) = new_tip {
            server.notify_subscriptions_chaintip(header);
        }
    }
    Ok(())
}

fn main() -> Result<()> {
    let config = Arc::new(Config::from_args());
    if let Err(e) = run_server(config) {
        info!(
            "Server stopped: {}",
            e.chain().map(|x| x.to_string()).collect::<String>()
        );
        if e.to_string().contains("Shutdown triggered")
            || e.to_string().contains("Interrupted by signal")
        {
            Ok(())
        } else {
            Err(e)
        }
    } else {
        Ok(())
    }
}
