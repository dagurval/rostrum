use crate::chaindef::OutPointHash;
use crate::store::Row;
use bitcoincash::hashes::Hash;
use bitcoincash::Txid;

use super::DBRow;

const INPUTINDEX_CF: &str = "input";

#[derive(Serialize, Deserialize)]
pub struct InputIndexKey {
    pub outpointhash: [u8; 32],
}

#[derive(Serialize, Deserialize)]
pub struct InputIndexRow {
    key: InputIndexKey,
    txid: [u8; 32],
    index: u32,
}

impl InputIndexRow {
    pub fn new(outpointhash: [u8; 32], txid: Txid, input_index: u32) -> InputIndexRow {
        InputIndexRow {
            key: InputIndexKey { outpointhash },
            txid: txid.into_inner(),
            index: input_index,
        }
    }

    pub fn filter_by_outpointhash(outpointhash: &OutPointHash) -> Vec<u8> {
        bincode::serialize(&InputIndexKey {
            outpointhash: outpointhash.into_inner(),
        })
        .unwrap()
    }

    pub fn txid(&self) -> Txid {
        Txid::from_inner(self.txid)
    }

    pub fn index(&self) -> u32 {
        self.index
    }
}

impl DBRow for InputIndexRow {
    const CF: &'static str = INPUTINDEX_CF;
    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap().into_boxed_slice(),
            value: bincode::serialize(&(self.txid, self.index))
                .unwrap()
                .into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> InputIndexRow {
        let (txid, index): ([u8; 32], u32) =
            bincode::deserialize(&row.value).expect("failed to parse OutputIndexRow");
        InputIndexRow {
            key: bincode::deserialize(&row.key).expect("failed to parse OutputIndexKey"),
            txid,
            index,
        }
    }
}
