use crate::bch::cashaddr::version_byte_flags as bch_flags;
use crate::chaindef::BlockHash;
use crate::chaindef::BlockHeader;
use crate::chaindef::ScriptHash;
use crate::chaindef::Transaction;
use crate::chaindef::TxOut;
use crate::doslimit::ConnectionLimits;
use crate::encode::compute_outpoint_hash_from_tx;
use crate::errors::rpc_invalid_params;
use crate::indexes::scripthashindex::QueryFilter;
use crate::mempool::MEMPOOL_HEIGHT;
use crate::nexa::cashaddr::version_byte_flags as nexa_flags;
use crate::query::status::hash_scripthash_history;
use crate::query::status::scripthash_history;
use crate::query::Query;
use crate::rpc::parseutil::{
    bool_from_value_or, hash_from_value, str_from_value, usize_from_value, usize_from_value_or,
};
use crate::rpc::rpcstats::RpcStats;
use crate::rpc::scripthash::{get_balance, get_first_use, get_history, get_mempool, listunspent};
use crate::scripthash::addr_to_scripthash;
use crate::scripthash::decode_address;
use crate::timeout::TimeoutTrigger;
use anyhow::{Context, Result};
use bitcoincash::consensus::encode::{deserialize, serialize};
use bitcoincash::hash_types::Txid;
use bitcoincash::hashes::hex::ToHex;
use bitcoincash::Network;
use serde_json::Value;
use std::collections::HashMap;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Arc, Mutex};
use std::time::Duration;

use super::parseutil::filter_from_value_or;

struct Subscription {
    statushash: Option<[u8; 32]>,
    alias: Option<String>,
}

pub struct BlockchainRpc {
    query: Arc<Query>,
    stats: Arc<RpcStats>,
    subscriptions: Mutex<HashMap<ScriptHash, Subscription>>,
    last_header_entry: Mutex<Option<BlockHeader>>,
    doslimits: ConnectionLimits,

    #[allow(dead_code)] // used in nex, but not bch
    network: Network,

    /* Resource tracking */
    alias_bytes_used: AtomicUsize,
}

impl BlockchainRpc {
    pub fn new(
        query: Arc<Query>,
        stats: Arc<RpcStats>,
        doslimits: ConnectionLimits,
        network: Network,
    ) -> BlockchainRpc {
        BlockchainRpc {
            query,
            stats,
            subscriptions: Mutex::new(HashMap::new()),
            last_header_entry: Mutex::new(None), // disable header subscription for now
            doslimits,
            network,
            alias_bytes_used: AtomicUsize::new(0),
        }
    }

    pub(crate) fn address_decode(&self, params: &[Value]) -> Result<Value> {
        let address = str_from_value(params.get(0), "address")?;
        let (payload, payload_type) = decode_address(&address)?;

        assert_eq!(bch_flags::TYPE_P2PKH, nexa_flags::TYPE_P2PKH);
        assert_eq!(bch_flags::TYPE_P2SH, nexa_flags::TYPE_P2SH);

        Ok(json!({
            "payload": payload.to_hex(),
            "type": match payload_type {
                bch_flags::TYPE_P2PKH | bch_flags::TYPE_P2PKH_TOKEN => "p2pkh",
                bch_flags::TYPE_P2SH | bch_flags::TYPE_P2SH_TOKEN => "p2sh",
                nexa_flags::TYPE_SCRIPT_TEMPLATE => "scripttemplate",
                nexa_flags::TYPE_GROUP => "group",
                _=> "Unknown"
            },
            "is_token_aware": vec![bch_flags::TYPE_P2PKH_TOKEN, bch_flags::TYPE_P2SH_TOKEN, nexa_flags::TYPE_SCRIPT_TEMPLATE, nexa_flags::TYPE_GROUP].contains(&payload_type)
        }))
    }

    pub fn address_get_balance(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        let scripthash = addr_to_scripthash(&addr)?;
        get_balance(&self.query, scripthash, filter, timeout)
    }
    pub fn address_get_first_use(&self, params: &[Value]) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        let scripthash = addr_to_scripthash(&addr)?;
        get_first_use(&self.query, scripthash, filter)
    }
    pub fn address_get_history(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        let scripthash = addr_to_scripthash(&addr)?;

        get_history(
            self.query.confirmed_index(),
            self.query.unconfirmed_index(),
            scripthash,
            filter,
            timeout,
        )
    }

    pub fn address_get_mempool(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        let scripthash = addr_to_scripthash(&addr)?;
        get_mempool(&self.query, scripthash, filter, timeout)
    }

    pub fn address_get_scripthash(&self, params: &[Value]) -> Result<Value> {
        let scripthash = addr_to_scripthash(&str_from_value(params.get(0), "address")?)?;
        Ok(json!(scripthash))
    }

    pub fn address_listunspent(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        let scripthash = addr_to_scripthash(&addr)?;
        listunspent(&self.query, scripthash, filter, timeout)
    }

    pub fn address_subscribe(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let scripthash = addr_to_scripthash(&addr)?;
        self.remove_subscription(&scripthash);

        self.doslimits
            .check_subscriptions(self.get_num_subscriptions() as u32 + 1)?;

        self.doslimits
            .check_alias_usage(self.alias_bytes_used.load(Ordering::Relaxed) + addr.len())?;

        let statushash = hash_scripthash_history(&scripthash_history(
            self.query.confirmed_index(),
            self.query.unconfirmed_index(),
            scripthash,
            QueryFilter::IncludeAll,
            None,
            timeout,
        )?);

        let result = statushash.map_or(Value::Null, |h| json!(hex::encode(h)));

        // We don't hold a lock on alias usage, so we could exceed limit here.
        // That's OK, it doesn't need to be a hard limit.
        self.alias_bytes_used
            .fetch_add(addr.len(), Ordering::Relaxed);
        self.subscriptions.lock().unwrap().insert(
            scripthash,
            Subscription {
                statushash,
                alias: Some(addr),
            },
        );
        self.stats.subscriptions.inc();
        Ok(result)
    }

    pub fn address_unsubscribe(&self, params: &[Value]) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let scripthash = addr_to_scripthash(&addr)?;
        Ok(json!(self.remove_subscription(&scripthash)))
    }

    /**
     * Get the raw block at given height or hash
     */
    pub(crate) fn block_get(&self, params: &[Value]) -> Result<Value> {
        if let Ok(blockhash) = hash_from_value::<BlockHash>(params.get(0)) {
            let block = self.query.get_block(&blockhash)?;
            return Ok(json!(hex::encode(serialize(&block))));
        }
        if let Ok(height) = usize_from_value(params.get(0), "height") {
            let header = self
                .query
                .header()
                .at_height(height)
                .ok_or_else(|| rpc_invalid_params("Invalid block height".to_string()))?;
            let block = self.query.get_block(&header.block_hash())?;
            return Ok(json!(hex::encode(serialize(&block))));
        }
        Err(rpc_invalid_params(
            "Argument must be hex encoded blockhash or numeric height".to_string(),
        ))
    }

    pub fn block_header(&self, params: &[Value]) -> Result<Value> {
        let height = usize_from_value(params.get(0), "height")?;
        let cp_height = usize_from_value_or(params.get(1), "cp_height", 0)?;

        let raw_header_hex: String = self
            .query
            .get_headers(&[height])
            .into_iter()
            .map(|entry| hex::encode(serialize(&entry)))
            .collect();

        if cp_height == 0 {
            return Ok(json!(raw_header_hex));
        }
        let (branch, root) = self.query.get_header_merkle_proof(height, cp_height)?;

        let branch_vec: Vec<String> = branch.into_iter().map(|b| b.to_hex()).collect();

        Ok(json!({
            "header": raw_header_hex,
            "root": root.to_hex(),
            "branch": branch_vec
        }))
    }

    pub fn block_headers(&self, params: &[Value]) -> Result<Value> {
        let start_height = usize_from_value(params.get(0), "start_height")?;
        let count = usize_from_value(params.get(1), "count")?;
        let cp_height = usize_from_value_or(params.get(2), "cp_height", 0)?;
        let heights: Vec<usize> = (start_height..(start_height + count)).collect();
        let headers: Vec<String> = self
            .query
            .get_headers(&heights)
            .into_iter()
            .map(|entry| hex::encode(serialize(&entry)))
            .collect();

        if count == 0 || cp_height == 0 {
            return Ok(json!({
                "count": headers.len(),
                "hex": headers.join(""),
                "max": 2016,
            }));
        }

        let (branch, root) = self
            .query
            .get_header_merkle_proof(start_height + (count - 1), cp_height)?;

        let branch_vec: Vec<String> = branch.into_iter().map(|b| b.to_hex()).collect();

        Ok(json!({
            "count": headers.len(),
            "hex": headers.join(""),
            "max": 2016,
            "root": root.to_hex(),
            "branch" : branch_vec
        }))
    }

    pub fn estimatefee(&self, params: &[Value]) -> Result<Value> {
        let blocks_count = usize_from_value(params.get(0), "blocks_count")?;
        let fee_rate = self.query.estimate_fee(blocks_count); // in BCH/kB
        Ok(json!(fee_rate.max(self.query.get_relayfee()?)))
    }

    pub fn headers_tip(&self) -> Result<Value> {
        let (height, header) = self.query.get_best_header()?;
        let hex = hex::encode(serialize(&header));
        Ok(json!({"hex":hex, "height":height}))
    }

    pub fn headers_subscribe(&self) -> Result<Value> {
        let (height, header) = self.query.get_best_header()?;
        let hex = hex::encode(serialize(&header));
        let result = json!({"hex":hex, "height":height});
        let mut last_entry = self.last_header_entry.lock().unwrap();
        *last_entry = Some(header);
        Ok(result)
    }

    pub fn relayfee(&self) -> Result<Value> {
        Ok(json!(self.query.get_relayfee()?)) // in BTC/kB
    }

    pub fn scripthash_get_balance(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        get_balance(&self.query, scripthash, filter, timeout)
    }

    pub fn scripthash_get_first_use(&self, params: &[Value]) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        get_first_use(&self.query, scripthash, filter)
    }

    pub fn scripthash_get_history(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        get_history(
            self.query.confirmed_index(),
            self.query.unconfirmed_index(),
            scripthash,
            filter,
            timeout,
        )
    }

    pub fn scripthash_get_mempool(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        get_mempool(&self.query, scripthash, filter, timeout)
    }

    pub fn scripthash_listunspent(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        let filter = filter_from_value_or(params.get(1), QueryFilter::IncludeAll)?;
        listunspent(&self.query, scripthash, filter, timeout)
    }

    pub fn scripthash_subscribe(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        self.remove_subscription(&scripthash);

        self.doslimits
            .check_subscriptions(self.get_num_subscriptions() as u32 + 1)?;

        let statushash = hash_scripthash_history(&scripthash_history(
            self.query.confirmed_index(),
            self.query.unconfirmed_index(),
            scripthash,
            QueryFilter::IncludeAll,
            None,
            timeout,
        )?);

        let result = statushash.map_or(Value::Null, |h| json!(hex::encode(h)));
        self.subscriptions.lock().unwrap().insert(
            scripthash,
            Subscription {
                statushash,
                alias: None,
            },
        );
        self.stats.subscriptions.inc();
        Ok(result)
    }

    pub fn scripthash_unsubscribe(&self, params: &[Value]) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        Ok(json!(self.remove_subscription(&scripthash)))
    }

    pub fn transaction_broadcast(&self, params: &[Value]) -> Result<Value> {
        let tx = params
            .get(0)
            .context(rpc_invalid_params("missing tx".to_string()))?;
        let tx = tx
            .as_str()
            .context(rpc_invalid_params("non-string tx".to_string()))?;
        let tx = hex::decode(tx).context(rpc_invalid_params("non-hex tx".to_string()))?;
        let tx: Transaction =
            deserialize(&tx).context(rpc_invalid_params("failed to parse tx".to_string()))?;
        let txid = self
            .query
            .broadcast(&tx)
            .context(rpc_invalid_params("rejected by network".to_string()))?;
        Ok(json!(txid.to_hex()))
    }

    pub fn transaction_get(&self, params: &[Value]) -> Result<Value> {
        let tx_hash = hash_from_value::<Txid>(params.get(0))?;
        let verbose = match params.get(1) {
            Some(value) => value.as_bool().context("non-bool verbose value")?,
            None => false,
        };
        if !verbose {
            let tx = self.query.tx().get(&tx_hash, None, None)?;
            Ok(json!(hex::encode(serialize(&tx))))
        } else {
            self.query.tx().get_verbose(&tx_hash)
        }
    }

    pub fn transaction_get_confirmed_blockhash(&self, params: &[Value]) -> Result<Value> {
        let tx_hash = hash_from_value(params.get(0)).context("bad tx_hash")?;
        self.query.get_confirmed_blockhash(tx_hash)
    }

    pub fn transaction_get_merkle(&self, params: &[Value]) -> Result<Value> {
        let tx_hash = hash_from_value::<Txid>(params.get(0))?;
        let height = if params.get(1).is_some() {
            usize_from_value(params.get(1), "height")
        } else {
            let header = self.query.header().get_by_txid(tx_hash, None);
            match header {
                Ok(Some(header)) => {
                    let height = self
                        .query
                        .header()
                        .get_height(&header)
                        .context(anyhow!("Block height for {} missing", header.block_hash()))?;
                    Ok(height as usize)
                }
                _ => Err(rpc_invalid_params(
                    "Transaction is not confirmed in a block".to_string(),
                )),
            }
        }?;

        let (merkle, pos) = self
            .query
            .get_merkle_proof(&tx_hash, height)
            .context("cannot create merkle proof")?;
        let merkle: Vec<String> = merkle.into_iter().map(|txid| txid.to_hex()).collect();
        Ok(json!({
                "block_height": height,
                "merkle": merkle,
                "pos": pos}))
    }

    pub fn transaction_id_from_pos(&self, params: &[Value]) -> Result<Value> {
        let height = usize_from_value(params.get(0), "height")?;
        let tx_pos = usize_from_value(params.get(1), "tx_pos")?;
        let want_merkle = bool_from_value_or(params.get(2), "merkle", false)?;

        let (txid, merkle) = self.query.get_id_from_pos(height, tx_pos, want_merkle)?;

        if !want_merkle {
            return Ok(json!(txid.to_hex()));
        }

        let merkle_vec: Vec<String> = merkle.into_iter().map(|entry| entry.to_hex()).collect();

        Ok(json!({
            "tx_hash" : txid.to_hex(),
            "merkle" : merkle_vec}))
    }

    #[cfg(feature = "nexa")]
    pub fn utxo_get(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        use crate::{
            nexa::transaction::{parse_script_template, TxOutType},
            rpc::parseutil::param_to_outpointhash,
        };
        let outpoint = param_to_outpointhash(params.get(0))?;
        let (tx, output_index, _) = match self.query.get_tx_funding_prevout(&outpoint)? {
            Some(found) => found,
            None => return Err(rpc_invalid_params("Outpoint not found".to_string())),
        };
        timeout.check()?;

        let (mut value, utxo) = self.utxo_get_inner(&tx, output_index)?;
        {
            let v = value.as_object_mut().unwrap();
            v.insert("tx_idem".to_string(), json!(tx.txidem().to_hex()));
            v.insert("tx_hash".to_string(), json!(tx.txid().to_hex()));
            v.insert("tx_pos".to_string(), json!(output_index));

            let template = if utxo.txout_type == TxOutType::TEMPLATE as u8 {
                parse_script_template(&utxo.script_pubkey)
            } else {
                None
            };

            v.insert(
                "group".to_string(),
                json!(template.as_ref().map(|t| t
                    .token
                    .as_ref()
                    .map(|token| token.as_cashaddr(self.network)))),
            );
            v.insert(
                "token_id_hex".to_string(),
                json!(template
                    .as_ref()
                    .map(|t| t.token.as_ref().map(|token| token.to_hex()))),
            );
            v.insert(
                "group_quantity".to_string(),
                json!(template.as_ref().map(|t| t.quantity)),
            );
            v.insert(
                "group_authority".to_string(),
                json!(template.as_ref().map(|t| t.group_authority().unwrap_or(0))),
            );
            v.insert(
                "template_scripthash".to_string(),
                json!(template.as_ref().map(|t| t.scripthash_human())),
            );
            v.insert(
                "template_argumenthash".to_string(),
                json!(template.map(|t| t.argumenthash.to_hex())),
            );
        }

        Ok(value)
    }

    #[cfg(not(feature = "nexa"))]
    pub fn utxo_get(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        let txid = hash_from_value::<Txid>(params.get(0))?;
        let out_n = usize_from_value(params.get(1), "out_n")?;
        if out_n > u32::MAX as usize {
            return Err(rpc_invalid_params(format!(
                "Too large value for out_n parameter ({} > {})",
                out_n,
                u32::MAX
            )));
        }
        let utxo_creation_tx = self.query.tx().get(&txid, None, None)?;
        timeout.check()?;
        let (mut value, utxo) = self.utxo_get_inner(&utxo_creation_tx, out_n as u32)?;
        {
            let v = value.as_object_mut().unwrap();
            v.insert(
                "token_id".to_string(),
                json!(utxo.token.as_ref().map(|t| t.id.to_hex())),
            );
            v.insert(
                "commitment".to_string(),
                json!(utxo.token.as_ref().map(|t| t.commitment.to_hex())),
            );
            v.insert(
                "token_amount".to_string(),
                json!(utxo.token.as_ref().map(|t| t.amount)),
            );
            v.insert(
                "token_bitfield".to_string(),
                json!(utxo.token.as_ref().map(|t| t.bitfield)),
            );
        }
        Ok(value)
    }

    fn utxo_get_inner(&self, utxo_creation_tx: &Transaction, out_n: u32) -> Result<(Value, TxOut)> {
        // We want to provide the utxo amount regardless of if it's spent or
        // unspent.

        let utxo = match utxo_creation_tx.output.get(out_n as usize) {
            Some(utxo) => utxo,
            None => {
                bail!(rpc_invalid_params(format!(
                    "out_n {} does not exist on tx {}, the transaction has {} outputs",
                    out_n,
                    utxo_creation_tx.txid(),
                    utxo_creation_tx.output.len()
                )));
            }
        };

        // Fetch the spending transaction (if the utxo is spent).
        let utxo_spending_tx = self
            .query
            .get_tx_spending_prevout(&compute_outpoint_hash_from_tx(utxo_creation_tx, out_n))?;

        let status = if utxo_spending_tx.is_some() {
            "spent"
        } else {
            "unspent"
        };

        let spent_json = match utxo_spending_tx {
            Some((tx, input_index, height)) => {
                json!({
                    "tx_hash": Some(tx.txid().to_string()),
                    "tx_pos": Some(input_index),
                    "height": Some(if height == MEMPOOL_HEIGHT { 0 } else { height}),
                })
            }
            None => {
                json!({
                    "tx_hash": None::<String>,
                    "tx_pos": None::<u32>,
                    "height": None::<i64>,
                })
            }
        };

        let utxo_confirmation_height = self
            .query
            .tx()
            .get_confirmation_height(utxo_creation_tx.txid());
        let utxo_scripthash = ScriptHash::from_script(&utxo.script_pubkey);

        Ok((
            json!({
                "status": status,
                "amount": utxo.value,
                "scriptpubkey": utxo.script_pubkey,
                "scripthash": utxo_scripthash,
                "height": utxo_confirmation_height,
                "spent": spent_json,
            }),
            utxo.clone(),
        ))
    }

    pub fn on_chaintip_change(&self, chaintip: BlockHeader) -> Result<Option<Value>> {
        let timer = self
            .stats
            .latency
            .with_label_values(&["chaintip_update"])
            .start_timer();

        let mut last_entry = self.last_header_entry.lock().unwrap();
        if last_entry.is_none() {
            return Ok(None);
        }
        if last_entry.as_ref() == Some(&chaintip) {
            return Ok(None);
        }

        let height = self.query.header().get_height(&chaintip).unwrap();
        *last_entry = Some(chaintip);
        let hex_header = hex::encode(serialize(&last_entry.as_ref().unwrap()));
        let header = json!({"hex": hex_header, "height": height});
        timer.observe_duration();
        Ok(Some(json!({
            "jsonrpc": "2.0",
            "method": "blockchain.headers.subscribe",
            "params": [header]})))
    }

    pub fn on_scripthash_change(&self, scripthash: ScriptHash) -> Result<Option<Value>> {
        let old_statushash: Option<[u8; 32]>;
        let subscription_name: String;
        let method: &str;

        let mut subscriptions = self.subscriptions.lock().unwrap();

        match subscriptions.get(&scripthash) {
            Some(subscription) => {
                old_statushash = subscription.statushash;
                if let Some(alias) = &subscription.alias {
                    subscription_name = alias.clone();
                    method = "blockchain.address.subscribe";
                } else {
                    subscription_name = scripthash.to_hex();
                    method = "blockchain.scripthash.subscribe";
                }
            }
            None => {
                return Ok(None);
            }
        };

        let timer = self
            .stats
            .latency
            .with_label_values(&["statushash_update"])
            .start_timer();

        let timeout = TimeoutTrigger::new(Duration::from_secs(self.doslimits.rpc_timeout as u64));

        let new_statushash = hash_scripthash_history(&scripthash_history(
            self.query.confirmed_index(),
            self.query.unconfirmed_index(),
            scripthash,
            QueryFilter::IncludeAll,
            None,
            &timeout,
        )?);

        if new_statushash == old_statushash {
            return Ok(None);
        }
        let new_statushash_hex = new_statushash.map_or(Value::Null, |h| json!(hex::encode(h)));
        let notification = Some(json!({
                    "jsonrpc": "2.0",
                    "method": method,
                    "params": [subscription_name, new_statushash_hex]}));
        subscriptions.get_mut(&scripthash).unwrap().statushash = new_statushash;
        timer.observe_duration();
        Ok(notification)
    }

    pub fn get_num_subscriptions(&self) -> i64 {
        self.subscriptions.lock().unwrap().len() as i64
    }

    fn remove_subscription(&self, scripthash: &ScriptHash) -> bool {
        let removed = self.subscriptions.lock().unwrap().remove(scripthash);
        match removed {
            Some(subscription) => {
                if let Some(alias) = subscription.alias {
                    self.alias_bytes_used
                        .fetch_sub(alias.len(), Ordering::Relaxed);
                }
                self.stats.subscriptions.dec();
                true
            }
            None => false,
        }
    }
}
