use anyhow::{Context, Result};

use crate::{
    chaindef::net_magic,
    def::ROSTRUM_VERSION,
    errors::ConnectionError,
    metrics,
    signal::{NetworkNotifier, Waiter},
};
use bitcoincash::{
    consensus::{
        encode::{self, ReadExt, VarInt},
        Decodable,
    },
    network::{
        address,
        message::{self, CommandString, NetworkMessage},
        message_blockdata::Inventory,
        message_network,
    },
    Block, Network, Txid,
};
use crossbeam_channel::{bounded, select};
use rand::prelude::*;

use std::net::{IpAddr, Ipv4Addr, SocketAddr, TcpStream};
use std::sync::Arc;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};
use std::{
    collections::HashSet,
    io::{self, ErrorKind, Write},
    sync::atomic::AtomicBool,
    sync::atomic::Ordering,
};

use prometheus::HistogramVec;

pub struct P2PConnection {
    is_broken: Arc<AtomicBool>,
}

impl P2PConnection {
    pub(crate) fn connect(
        network: Network,
        address: SocketAddr,
        network_signals: Arc<NetworkNotifier>,
        send_duration: Arc<HistogramVec>,
        recv_duration: Arc<HistogramVec>,
        parse_duration: Arc<HistogramVec>,
        recv_size: Arc<HistogramVec>,
    ) -> Result<Self, ConnectionError> {
        let conn = Arc::new(match TcpStream::connect(address) {
            Ok(c) => c,
            Err(err) => {
                return Err(ConnectionError {
                    msg: format!("p2p failed to connect to {:?} {}", address, err),
                })
            }
        });

        let (tx_send, tx_recv) = bounded::<NetworkMessage>(1);
        let (rx_send, rx_recv) = bounded::<RawNetworkMessage>(1);

        let stream = Arc::clone(&conn);
        let is_broken: Arc<AtomicBool> = Arc::new(AtomicBool::new(false));
        let is_broken_copy = Arc::clone(&is_broken);
        crate::thread::spawn("p2p_send", move || loop {
            Waiter::shutdown_check()?;
            use std::net::Shutdown;
            let msg = match metrics::observe_duration(&send_duration, "wait", || tx_recv.recv()) {
                Ok(msg) => msg,
                Err(_) => {
                    is_broken_copy.store(true, Ordering::Relaxed);
                    // p2p_loop is closed, so tx_send is disconnected
                    debug!("closing p2p_send thread: no more messages to send");
                    // close the stream reader (p2p_recv thread may block on it)
                    if let Err(e) = stream.shutdown(Shutdown::Read) {
                        debug!("failed to shutdown p2p connection: {}", e)
                    }
                    return Ok(());
                }
            };
            match metrics::observe_duration(&send_duration, "send", || {
                trace!("send: {:?}", msg);
                let raw_msg = message::RawNetworkMessage {
                    magic: net_magic(network),
                    payload: msg,
                };
                (&*stream)
                    .write_all(encode::serialize(&raw_msg).as_slice())
                    .context("p2p failed to send")
            }) {
                Ok(_) => {}
                Err(err) => {
                    is_broken_copy.store(true, Ordering::Relaxed);
                    return Err(err);
                }
            };
        });

        let stream = Arc::clone(&conn);
        let is_broken_copy = Arc::clone(&is_broken);
        crate::thread::spawn("p2p_recv", move || loop {
            Waiter::shutdown_check()?;
            let start = Instant::now();
            let raw_msg = RawNetworkMessage::consensus_decode(&mut &*stream);
            {
                let duration = duration_to_seconds(start.elapsed());
                let label = format!(
                    "recv_{}",
                    raw_msg
                        .as_ref()
                        .map(|msg| msg.cmd.as_ref())
                        .unwrap_or("err")
                );
                metrics::observe(&recv_duration, &label, duration);
            }
            let raw_msg = match raw_msg {
                Ok(raw_msg) => {
                    metrics::observe(&recv_size, raw_msg.cmd.as_ref(), raw_msg.raw.len() as f64);
                    if raw_msg.magic != net_magic(network) {
                        return Err(anyhow!(
                            "unexpected magic {} (instead of {})",
                            raw_msg.magic,
                            net_magic(network)
                        ));
                    }
                    raw_msg
                }
                Err(encode::Error::Io(e)) if e.kind() == ErrorKind::UnexpectedEof => {
                    debug!("closing p2p_recv thread: connection closed");
                    is_broken_copy.store(true, Ordering::Relaxed);
                    return Ok(());
                }
                Err(e) => {
                    debug!("failed to recv a message from peer: {}", e);
                    is_broken_copy.store(true, Ordering::Relaxed);
                    return Ok(());
                }
            };

            metrics::observe_duration(&recv_duration, "wait", || rx_send.send(raw_msg))?;
        });

        let (init_send, init_recv) = bounded::<()>(0);

        match tx_send.send(build_version_message()) {
            Ok(_) => {}
            Err(err) => {
                return Err(ConnectionError {
                    msg: format!("p2p failed to send msg: {}", err),
                })
            }
        }

        let is_broken_copy = Arc::clone(&is_broken);
        crate::thread::spawn("p2p_loop", move || loop {
            Waiter::shutdown_check()?;
            select! {
                recv(rx_recv) -> result => {
                    let raw_msg = match result {
                        Ok(raw_msg) => raw_msg,
                        Err(_) => {  // p2p_recv is closed, so rx_send is disconnected
                            debug!("closing p2p_loop thread: peer has disconnected");
                            is_broken_copy.store(true, Ordering::Relaxed);
                            return Ok(());

                        }
                    };

                    let label = format!("parse_{}", raw_msg.cmd.as_ref());
                    let msg = match metrics::observe_duration(&parse_duration, &label, || raw_msg.parse()) {
                        Ok(msg) => msg,
                        Err(err) => {
                            is_broken_copy.store(true, Ordering::Relaxed);
                            debug!("failed to parse '{}({:?})': {}", raw_msg.cmd, raw_msg.raw, err);
                            return Ok(());
                        }
                    };

                    match msg {
                        NetworkMessage::GetHeaders(_) => {
                            match tx_send.send(NetworkMessage::Headers(vec![])) {
                                Ok(_) => {},
                                Err(err) => {
                                    is_broken_copy.store(true, Ordering::Relaxed);
                                    debug!("failed to reply to getheaders: {}", err);
                                    return Ok(())
                                }
                            }
                        }
                        NetworkMessage::Version(version) => {
                            debug!("peer version: {:?}", version);
                            match tx_send.send(NetworkMessage::Verack) {
                                Ok(_) => {},
                                Err(err) => {
                                    is_broken_copy.store(true, Ordering::Relaxed);
                                    debug!("failed to reply to version: {}", err);
                                    return Ok(())
                                }
                            }
                        }
                        NetworkMessage::Inv(inventory) => {
                            let mut new_txs: HashSet<Txid> = HashSet::new();
                            for inv in inventory {
                                match inv {
                                    Inventory::Transaction(txid) => {
                                        new_txs.insert(txid);
                                    }
                                    Inventory::Block(blockhash) | Inventory::CompactBlock(blockhash) => {
                                        // best-effort notification
                                        #[cfg(feature = "nexa")]
                                        {
                                            use bitcoin_hashes::Hash;
                                            // TODO: Fix this hash conversion hack
                                            let blockhash = crate::nexa::hash_types::BlockHash::from_inner(blockhash.into_inner());
                                            if let Err(e) = network_signals.block_sender.try_send(blockhash) {
                                                trace!("Failed to notify of block: {}", e);
                                            }
                                        }
                                        #[cfg(not(feature = "nexa"))]
                                        {
                                            let _ = network_signals.block_sender.try_send(blockhash);
                                        }
                                    }
                                    _ => { /* ignore */}
                                }
                            }
                            // best effort
                            if let Err(e) = network_signals.txid_sender.try_send(new_txs) {
                                trace!("Failed to notify of inv: {}", e);
                            }

                        },
                        NetworkMessage::Ping(nonce) => {
                            // connection keep-alive
                            match tx_send.send(NetworkMessage::Pong(nonce)) {
                                Ok(_) => {},
                                Err(err) => {
                                    is_broken_copy.store(true, Ordering::Relaxed);
                                    debug!("Failed to respond to ping: {}", err);
                                    return Ok(())
                                }
                            };
                        }
                        NetworkMessage::Verack => {
                             // peer acknowledged our version
                            match init_send.send(()){
                                Ok(_) => {},
                                Err(err) => {
                                    is_broken_copy.store(true, Ordering::Relaxed);
                                    return Err(err.into());
                                }
                            };
                        }
                        NetworkMessage::Block(_) => (),
                        NetworkMessage::Headers(_) => (),
                        NetworkMessage::Alert(_) => (),  // https://bitcoin.org/en/alert/2016-11-01-alert-retirement
                        NetworkMessage::Addr(_) => (),   // unused
                        msg => debug!("unexpected message: {:?}", msg),
                    }
                }
            }
        });

        // wait until `verack` is received
        match init_recv.recv() {
            Ok(_) => {}
            Err(err) => {
                return Err(ConnectionError {
                    msg: format!("p2p failed to receive verack: {}", err),
                })
            }
        };

        Ok(P2PConnection { is_broken })
    }

    pub fn is_broken(&self) -> bool {
        Waiter::shutdown_check().is_err() || self.is_broken.load(Ordering::Relaxed)
    }
}

fn build_version_message() -> NetworkMessage {
    let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), 0);
    let timestamp = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time error")
        .as_secs() as i64;

    let services = bitcoincash::network::constants::ServiceFlags::NONE;

    #[cfg(not(feature = "nexa"))]
    let version = bitcoincash::network::constants::PROTOCOL_VERSION;
    #[cfg(feature = "nexa")]
    let version = 80003;

    NetworkMessage::Version(message_network::VersionMessage {
        version,
        services,
        timestamp,
        receiver: address::Address::new(&addr, services),
        sender: address::Address::new(&addr, services),
        nonce: rand::thread_rng().gen(),
        user_agent: format!("/rostrum:{}/", ROSTRUM_VERSION),
        start_height: 0,
        relay: true,
    })
}

struct RawNetworkMessage {
    magic: u32,
    cmd: CommandString,
    raw: Vec<u8>,
}

impl RawNetworkMessage {
    fn parse(&self) -> Result<NetworkMessage> {
        let mut raw: &[u8] = &self.raw;
        let payload = match self.cmd.as_ref() {
            "version" => NetworkMessage::Version(Decodable::consensus_decode(&mut raw)?),
            "verack" => NetworkMessage::Verack,
            "inv" => NetworkMessage::Inv(Decodable::consensus_decode(&mut raw)?),
            "notfound" => NetworkMessage::NotFound(Decodable::consensus_decode(&mut raw)?),
            "block" => NetworkMessage::Block(Decodable::consensus_decode(&mut raw)?),
            "headers" => {
                let len = VarInt::consensus_decode(&mut raw)?.0;
                let mut headers = Vec::with_capacity(len as usize);
                for _ in 0..len {
                    headers.push(Block::consensus_decode(&mut raw)?.header);
                }
                NetworkMessage::Headers(headers)
            }
            "ping" => NetworkMessage::Ping(Decodable::consensus_decode(&mut raw)?),
            "pong" => NetworkMessage::Pong(Decodable::consensus_decode(&mut raw)?),
            "reject" => NetworkMessage::Reject(Decodable::consensus_decode(&mut raw)?),
            "alert" => NetworkMessage::Alert(Decodable::consensus_decode(&mut raw)?),
            "addr" => NetworkMessage::Addr(Decodable::consensus_decode(&mut raw)?),
            _ => NetworkMessage::Unknown {
                command: self.cmd.clone(),
                payload: raw.to_vec(),
            },
        };
        Ok(payload)
    }
}

impl Decodable for RawNetworkMessage {
    fn consensus_decode<D: io::Read + ?Sized>(d: &mut D) -> Result<Self, encode::Error> {
        let magic = Decodable::consensus_decode(d)?;
        let cmd = Decodable::consensus_decode(d)?;

        let len = u32::consensus_decode(d)?;
        let _checksum = <[u8; 4]>::consensus_decode(d)?; // assume data is correct
        let mut raw = vec![0u8; len as usize];
        d.read_slice(&mut raw)?;

        Ok(RawNetworkMessage { magic, cmd, raw })
    }
}

/// `duration_to_seconds` converts Duration to seconds.
#[inline]
pub fn duration_to_seconds(d: Duration) -> f64 {
    let nanos = f64::from(d.subsec_nanos()) / 1e9;
    d.as_secs() as f64 + nanos
}
