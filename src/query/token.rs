use std::collections::HashSet;

use anyhow::Result;
#[cfg(not(features = "nexa"))]
use bitcoincash::Txid;

use crate::{
    chaindef::{TokenID, Transaction},
    indexes::{
        heightindex::HeightIndexRow, outputindex::OutputIndexRow,
        tokenoutputindex::TokenOutputIndexRow, DBRow,
    },
    store::DBStore,
    timeout::TimeoutTrigger,
};

use super::{queryutil::get_row, tx::TxQuery};

/**
 * Returns full transaction history for a token
 */
pub(crate) fn get_token_history(
    token_id: &TokenID,
    store: &DBStore,
    timeout: &TimeoutTrigger,
) -> Result<Vec<(Txid, u32)>> {
    let to_outpoint_filter = TokenOutputIndexRow::filter_by_token_id(token_id);
    let h = store
        .scan(TokenOutputIndexRow::CF, to_outpoint_filter)
        .map(|r| TokenOutputIndexRow::from_row(&r))
        .map(|r| {
            timeout.check()?;
            let txid_key = OutputIndexRow::filter_by_outpointhash(&r.outpoint_hash());
            let txid = get_row::<OutputIndexRow>(store, txid_key)
                .expect("txid not found for token output")
                .txid();
            let height_key = HeightIndexRow::filter_by_txid(&txid);
            let height = get_row::<HeightIndexRow>(store, height_key)
                .expect("height not found for txid")
                .height();
            Ok((txid, height))
        })
        .collect::<Result<Vec<(Txid, u32)>>>();

    let mut h = h?;

    // Sort by inverse height first, then txid.
    h.sort_unstable_by(|a, b| match b.1.cmp(&a.1) {
        std::cmp::Ordering::Less => std::cmp::Ordering::Less,
        std::cmp::Ordering::Equal => b.0.cmp(&a.0),
        std::cmp::Ordering::Greater => std::cmp::Ordering::Greater,
    });
    h.dedup();
    Ok(h)
}

/// Locates and returns the transaction that created a token.
#[cfg(not(feature = "nexa"))]
pub(crate) fn find_token_genesis(
    token_id: &TokenID,
    store: &DBStore,
    txquery: &TxQuery,
) -> Result<(Transaction, i64)> {
    use anyhow::Context;
    use bitcoin_hashes::Hash;

    use crate::{
        encode::compute_outpoint_hash, errors::rpc_invalid_params,
        indexes::inputindex::InputIndexRow,
    };

    // Token genesis on BCH is the transaction spending the first output
    // of the transaction that has the same value for its TxID as the TokenID.
    let genesis_outpoint = compute_outpoint_hash(&Txid::from_inner(token_id.into_inner()), 0);
    let spender = get_row::<InputIndexRow>(
        store,
        InputIndexRow::filter_by_outpointhash(&genesis_outpoint),
    )
    .context(rpc_invalid_params("Token genesis not found".to_string()))?;
    debug_assert!(spender.index() == 0);
    let genesis_txid = spender.txid();
    let height = txquery
        .get_confirmation_height(genesis_txid.clone())
        .context(rpc_invalid_params(
            "Genesis transaction not found".to_string(),
        ))?;
    Ok((
        txquery.get(
            &genesis_txid,
            None,
            if height > 0 {
                Some(height as u32)
            } else {
                None
            },
        )?,
        height,
    ))
}

/// Locates and returns the transaction that created a token.
#[cfg(feature = "nexa")]
pub(crate) fn find_token_genesis(
    token_id: &TokenID,
    store: &DBStore,
    txquery: &TxQuery,
) -> Result<(Transaction, i64)> {
    use crate::indexes::outputtokenindex::OutputTokenIndexRow;

    let to_outpoint_filter = TokenOutputIndexRow::filter_by_token_id(token_id);

    let lowest_height_txs: Vec<(Txid, u32)> = store
        .scan(TokenOutputIndexRow::CF, to_outpoint_filter)
        .map(|r| TokenOutputIndexRow::from_row(&r))
        .map(|r| {
            let txid_key = OutputIndexRow::filter_by_outpointhash(&r.outpoint_hash());
            let txid = get_row::<OutputIndexRow>(store, txid_key)
                .expect("txid not found for token output")
                .txid();
            let height_key = HeightIndexRow::filter_by_txid(&txid);
            let height = get_row::<HeightIndexRow>(store, height_key)
                .expect("height not found for txid")
                .height();
            (txid, height)
        })
        .fold(Vec::new(), |lowest, tx| {
            if lowest.is_empty() {
                vec![tx]
            } else {
                match tx.1.cmp(&lowest[0].1) {
                    std::cmp::Ordering::Less => vec![tx],
                    std::cmp::Ordering::Equal => {
                        lowest.into_iter().chain(std::iter::once(tx)).collect()
                    }
                    std::cmp::Ordering::Greater => lowest,
                }
            }
        });

    // One of the txs at lowest block height must be genesis, but we need to check which one.
    for (txid, height) in lowest_height_txs {
        // Mempool txs are expected to use MEMPOOL_HEIGHT
        assert!(height > 0);

        let tx = txquery.get(&txid, None, Some(height))?;

        // If any of the inputs contain token, then this tx is not the genesis.
        if tx.input.iter().any(|i| {
            let outpoint_hash = &i.previous_output.hash;
            let filter = OutputTokenIndexRow::filter_by_outpointhash_and_token(
                outpoint_hash,
                token_id.clone(),
            );
            store
                .scan(OutputTokenIndexRow::CF, filter)
                .peekable()
                .peek()
                .is_some()
        }) {
            continue;
        } else {
            // No inputs had token, this tx must be genesis.
            return Ok((tx, height as i64));
        }
    }
    bail!("Token {} not found", token_id.to_hex())
}

/**
 * List all subtokens / commitments for a a token.
 */
pub(crate) fn list_nft(
    token_id: &TokenID,
    store: &DBStore,
    timeout: &TimeoutTrigger,
) -> Result<HashSet<([u8; 32], Vec<u8>)>> {
    let to_outpoint_filter = TokenOutputIndexRow::filter_by_token_id_prefix(token_id);
    store
        .scan(TokenOutputIndexRow::CF, to_outpoint_filter)
        .map(|r| TokenOutputIndexRow::from_row(&r))
        .filter(|r| r.is_subtoken())
        .map(|r| {
            timeout.check()?;
            Ok(r.into_parent_and_subgroup())
        })
        .collect::<Result<HashSet<([u8; 32], Vec<u8>)>>>()
}
