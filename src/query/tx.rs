use crate::cache::TransactionCache;
use crate::chaindef::BlockHash;
use crate::chaindef::OutPointHash;
use crate::chaindef::Transaction;
use crate::chaindef::TxOut;
use crate::daemon::Daemon;
use crate::def::COIN;
use crate::encode::compute_outpoint_hash_from_tx;
use crate::encode::outpoint_hash;
use crate::indexes::outputindex::OutputIndexRow;
use crate::mempool::ConfirmationState;
use crate::mempool::MempoolEntry;
use crate::mempool::Tracker;

use crate::query::header::HeaderQuery;
use crate::rpc::encoding::blockhash_to_hex;
use crate::store::DBStore;
use anyhow::{Context, Result};
use bitcoincash::consensus::encode::{deserialize, serialize};
use bitcoincash::hash_types::Txid;
use bitcoincash::hashes::hex::ToHex;
use bitcoincash::network::constants::Network;
use bitcoincash::util::address::{Address, AddressType};
use rust_decimal::prelude::*;
use serde_json::Value;
use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::Arc;

use super::queryutil::get_row_by_key_prefix;

///  String returned is intended to be the same as produced by bitcoind
///  GetTxnOutputType
fn get_address_type(out: &TxOut, network: Network) -> Option<&str> {
    #[cfg(feature = "nexa")]
    {
        use crate::nexa::transaction::TxOutType;
        if out.txout_type == (TxOutType::TEMPLATE as u8) {
            return Some("scripttemplate");
        }
    }

    let script = &out.script_pubkey;
    if script.is_op_return() {
        return Some("nulldata");
    }
    let address = Address::from_script(script, network).ok()?;
    let address_type = address.address_type();
    match address_type {
        Some(AddressType::P2pkh) => Some("pubkeyhash"),
        Some(AddressType::P2sh) => Some("scripthash"),
        _ => {
            if !address.is_standard() {
                Some("nonstandard")
            } else {
                None
            }
        }
    }
}

#[cfg(not(feature = "nexa"))]
fn get_addresses(out: &TxOut, network: Network) -> Vec<String> {
    use crate::bch::cashaddr::encode as bchaddr_encode;
    use crate::bch::cashaddr::version_byte_flags;
    use bitcoincash::util::address::Payload::{PubkeyHash, ScriptHash};
    let script = &out.script_pubkey;
    let address = match Address::from_script(script, network).ok() {
        Some(a) => a,
        None => return vec![],
    };

    match address.payload {
        PubkeyHash(pubhash) => {
            let hash = pubhash.as_hash().to_vec();
            let encoded = bchaddr_encode(&hash, version_byte_flags::TYPE_P2PKH, network);
            let encoded_token =
                bchaddr_encode(&hash, version_byte_flags::TYPE_P2PKH_TOKEN, network);
            match encoded {
                Ok(addr) => {
                    // If it was P2PKH encoded, then it can also be token aware.
                    let addr_token = encoded_token.unwrap();
                    vec![addr, addr_token]
                }
                _ => vec![],
            }
        }
        ScriptHash(scripthash) => {
            let hash = scripthash.as_hash().to_vec();
            let encoded = bchaddr_encode(&hash, version_byte_flags::TYPE_P2SH, network);
            let encoded_token = bchaddr_encode(&hash, version_byte_flags::TYPE_P2SH_TOKEN, network);
            match encoded {
                Ok(addr) => {
                    // If it was P2SH encoded, then it can also be token aware.
                    let addr_token = encoded_token.unwrap();
                    vec![addr, addr_token]
                }
                _ => vec![],
            }
        }
        _ => vec![],
    }
}

#[cfg(feature = "nexa")]
fn get_addresses(output: &TxOut, network: Network) -> Vec<String> {
    use crate::nexa::cashaddr::encode as nexaaddr_encode;
    use crate::nexa::cashaddr::version_byte_flags;
    use crate::nexa::transaction::TxOutType;
    use bitcoincash::util::address::Payload::{PubkeyHash, ScriptHash};

    if output.txout_type == TxOutType::TEMPLATE as u8 {
        if let Ok(addr) = nexaaddr_encode(
            output.script_pubkey.as_bytes(),
            version_byte_flags::TYPE_SCRIPT_TEMPLATE,
            network,
        ) {
            return vec![addr];
        }
        return vec![];
    }

    if output.txout_type != TxOutType::SATOSHI as u8 {
        // unsupported output
        return vec![];
    }

    let address = match Address::from_script(&output.script_pubkey, network).ok() {
        Some(a) => a,
        None => return vec![],
    };

    match address.payload {
        PubkeyHash(pubhash) => {
            let hash = pubhash.as_hash().to_vec();
            if let Ok(addr) = nexaaddr_encode(&hash, version_byte_flags::TYPE_P2PKH, network) {
                return vec![addr];
            }
            vec![]
        }
        ScriptHash(scripthash) => {
            let hash = scripthash.as_hash().to_vec();
            if let Ok(addr) = nexaaddr_encode(&hash, version_byte_flags::TYPE_P2SH, network) {
                return vec![addr];
            }
            vec![]
        }
        _ => vec![],
    }
}

fn value_from_amount(amount: u64) -> Value {
    if amount == 0 {
        return json!(0.0);
    }
    let satoshis = Decimal::new(amount as i64, 0);
    // rust-decimal crate with feature 'serde-float' should make this work
    // without introducing precision errors
    json!(satoshis.checked_div(Decimal::new(COIN as i64, 0)).unwrap())
}

/**
 * Given an output, lookup how many satoshis were/are in the utxo.
 */
pub(crate) fn sats_from_outpoint(store: &DBStore, outpointhash: &OutPointHash) -> Option<u64> {
    let key = OutputIndexRow::filter_by_outpointhash(outpointhash);
    get_row_by_key_prefix::<OutputIndexRow>(store, key).map(|r| r.value())
}

pub struct TxQuery {
    tx_cache: TransactionCache,
    daemon: Daemon,
    mempool: Arc<Tracker>,
    header: Arc<HeaderQuery>,
    duration: Arc<prometheus::HistogramVec>,
    network: Network,
    confirmed_index: Arc<DBStore>,
}

impl TxQuery {
    pub fn new(
        tx_cache: TransactionCache,
        daemon: Daemon,
        mempool: Arc<Tracker>,
        header: Arc<HeaderQuery>,
        duration: Arc<prometheus::HistogramVec>,
        network: Network,
        confirmed_index: Arc<DBStore>,
    ) -> TxQuery {
        TxQuery {
            tx_cache,
            daemon,
            mempool,
            header,
            duration,
            network,
            confirmed_index,
        }
    }

    /// Get a transaction by Txid.
    pub fn get(
        &self,
        txid: &Txid,
        blockhash: Option<&BlockHash>,
        blockheight: Option<u32>,
    ) -> Result<Transaction> {
        if let Some(tx) = self.tx_cache.get(txid) {
            return Ok(tx);
        }
        let _timer = self.duration.with_label_values(&["load_txn"]).start_timer();

        let hash: Option<BlockHash> = match blockhash {
            Some(hash) => Some(*hash),
            None => match self.header.get_by_txid(*txid, blockheight) {
                Ok(header) => header.map(|h| h.block_hash()),
                Err(_) => None,
            },
        };
        self.load_txn_from_bitcoind(txid, hash.as_ref())
    }

    /// Get an transaction known to be unconfirmed.
    ///
    /// This is slightly faster than `get` as it avoids blockhash lookup. May
    /// or may not return the transaction even if it is confirmed.
    pub fn get_unconfirmed(&self, txid: &Txid) -> Result<Transaction> {
        if let Some(tx) = self.tx_cache.get(txid) {
            Ok(tx)
        } else {
            self.load_txn_from_bitcoind(txid, None)
        }
    }

    #[cfg(not(feature = "nexa"))]
    fn inputs_to_json(&self, tx: &Transaction) -> (Vec<Value>, u64) {
        let inputs = tx.input.iter().map(|txin| {

            let outpoint = outpoint_hash(&txin.previous_output);
            let value =
                sats_from_outpoint(&self.mempool.index(), &outpoint)
                .or_else(||
                    sats_from_outpoint(&self.confirmed_index, &outpoint)
                );
                (json!({
            // bitcoind adds scriptSig hex as 'coinbase' when the transaction is a coinbase
            "coinbase": if tx.is_coin_base() { Some(txin.script_sig.to_hex()) } else { None },
            "sequence": txin.sequence,
            "txid": txin.previous_output.txid.to_hex(),
            "vout": txin.previous_output.vout,
            "value_satoshi": value,
            "value_coin": value.map(value_from_amount),
            "scriptSig": {
                "asm": txin.script_sig.asm(),
                "hex": txin.script_sig.to_hex(),
            },
        }), value.unwrap_or(0))
    }).collect::<Vec<(Value, u64)>>();

        let total_value_in = inputs.iter().map(|(_, v)| v).sum();
        let vin = inputs.into_iter().map(|(i, _)| i).collect();
        (vin, total_value_in)
    }

    #[cfg(feature = "nexa")]
    fn inputs_to_json(&self, tx: &Transaction) -> (Vec<Value>, u64) {
        let inputs = tx.input.iter().map(|txin| {
            let outpoint = txin.previous_output.hash;
            let value =
                sats_from_outpoint(&self.mempool.index(), &outpoint)
                .or_else(||
                    sats_from_outpoint(&self.confirmed_index, &outpoint)
                );

            (
                json!({
                // bitcoind adds scriptSig hex as 'coinbase' when the transaction is a coinbase
                "coinbase": if tx.is_coin_base() { Some(txin.script_sig.to_hex()) } else { None },
                "sequence": txin.sequence,
                "outpoint": outpoint.to_hex(),
                "value_satoshi": value,
                "value_coin": value.map(value_from_amount),
                "value": value.map(value_from_amount),
                "scriptSig": {
                    "asm": txin.script_sig.asm(),
                    "hex": txin.script_sig.to_hex(),
                }}),
                value.unwrap_or(0)
            )
        }).collect::<Vec<(Value, u64)>>();

        let total_value = inputs.iter().map(|(_, value)| value).sum();
        let vin: Vec<Value> = inputs.into_iter().map(|(input, _)| input).collect();
        (vin, total_value)
    }

    #[cfg(feature = "nexa")]
    fn outputs_to_json(&self, tx: &Transaction) -> (Vec<Value>, u64) {
        use crate::{
            encode::compute_outpoint_hash,
            nexa::transaction::{parse_script_template, TxOutType},
        };

        let total_output = tx.output.iter().map(|o| o.value).sum();
        let txidem = tx.txidem();

        let vout = tx.output
            .iter()
            .enumerate()
            .map(|(n, txout)| {
                let template = if txout.txout_type == (TxOutType::TEMPLATE as u8) {
                    parse_script_template(&txout.script_pubkey)
                } else {
                    None
                };

                json!({
                    "type": txout.txout_type,
                    "value": value_from_amount(txout.value),
                    "value_satoshi": txout.value,
                    "value_coin": value_from_amount(txout.value),
                    "n": n,
                    "scriptPubKey": {
                        "asm": txout.script_pubkey.asm(),
                        "hex": txout.script_pubkey.to_hex(),
                        "type": get_address_type(txout, self.network).unwrap_or_default(),
                        "addresses": get_addresses(txout, self.network),
                        "groupQuantity": template.as_ref().map(|t| t.quantity),
                        "groupAuthority": template.as_ref().map(|t| t.group_authority().unwrap_or(0)),
                        "scriptHash": template.as_ref().map(|t| t.scripthash_human()),
                        "argsHash": template.as_ref().map(|t| t.argumenthash.to_hex()),
                        "group": template.as_ref().map(|t| t.token.as_ref().map(|id| id.as_cashaddr(self.network))),
                        "token_id_hex": template.map(|t| t.token.map(|id| id.to_hex())),
                    },
                    "outpoint_hash": compute_outpoint_hash(&txidem, n as u32),

                })
            })
            .collect::<Vec<Value>>();

        (vout, total_output)
    }

    #[cfg(not(feature = "nexa"))]
    fn outputs_to_json(&self, tx: &Transaction) -> (Vec<Value>, u64) {
        use bitcoincash::blockdata::token::Capability;

        let capability_str = |capability: u8| {
            if (capability & Capability::Minting as u8) != 0 {
                "minting"
            } else if (capability & Capability::Mutable as u8) != 0 {
                "mutable"
            } else {
                "none"
            }
        };

        let total_value_out: u64 = tx.output.iter().map(|o| o.value).sum();

        let vout = tx
            .output
            .iter()
            .enumerate()
            .map(|(n, txout)| {
                let token_data = txout.token.as_ref().map(|token| {
                    json!({
                        "category": token.id,
                        "amount": token.amount.to_string(),
                        "nft": if token.has_nft() {
                            Some(json!({
                                "commitment": token.commitment.to_hex(),
                                "capability": capability_str(token.capability()),
                            }))
                        } else {
                            None
                        }
                    })
                });
                json!({
                "value_satoshi": txout.value,
                "value_coin": value_from_amount(txout.value),
                "value": value_from_amount(txout.value),
                "n": n,
                "tokenData": token_data,
                "scriptPubKey": {
                    "asm": txout.script_pubkey.asm(),
                    "hex": txout.script_pubkey.to_hex(),
                    "type": get_address_type(txout, self.network).unwrap_or_default(),
                    "addresses": get_addresses(txout, self.network),
                },
                })
            })
            .collect::<Vec<Value>>();

        (vout, total_value_out)
    }

    pub fn get_verbose(&self, txid: &Txid) -> Result<Value> {
        let header = self.header.get_by_txid(*txid, None).unwrap_or_default();
        let blocktime = header.as_ref().map(|header| header.time);
        let height = header
            .as_ref()
            .map(|header| self.header.get_height(header).unwrap());
        let confirmations = match height {
            Some(ref height) => {
                let best = self.header.best();
                let best_height = self.header.get_height(&best).unwrap();

                Some(1 + best_height - height)
            }
            None => None,
        };
        let (blockhash, blockhash_hex) = if let Some(h) = header {
            let hash = h.block_hash();
            (Some(hash), Some(blockhash_to_hex(&hash)))
        } else {
            (None, None)
        };
        let tx = self.get(txid, blockhash.as_ref(), None)?;
        let tx_serialized = serialize(&tx);

        let (vin, total_value_in) = self.inputs_to_json(&tx);
        let (vout, total_value_out) = self.outputs_to_json(&tx);

        #[allow(unused_mut)]
        let mut tx_details = json!({
            "blockhash": blockhash_hex,
            "blocktime": blocktime,
            "height": height,
            "confirmations": confirmations,
            "hash": tx.txid().to_hex(),
            "txid": tx.txid().to_hex(),
            "size": tx_serialized.len(),
            "hex": hex::encode(tx_serialized),
            "locktime": tx.lock_time,
            "time": blocktime,
            "version": tx.version,
            "vin": vin,
            "vout": vout,
            "fee": value_from_amount(total_value_in.saturating_sub(total_value_out)),
            "fee_satoshi": total_value_in.saturating_sub(total_value_out),
        });

        #[cfg(feature = "nexa")]
        {
            tx_details
                .as_object_mut()
                .unwrap()
                .insert("txidem".to_string(), json!(tx.txidem().to_hex()));
        }

        Ok(json!(tx_details))
    }

    fn load_txn_from_bitcoind(
        &self,
        txid: &Txid,
        blockhash: Option<&BlockHash>,
    ) -> Result<Transaction> {
        let value: Value = self
            .daemon
            .gettransaction_raw(txid, blockhash, /*verbose*/ false)?;
        let value_hex: &str = value.as_str().context("non-string tx")?;
        let serialized_tx = hex::decode(value_hex).context("non-hex tx")?;
        let tx: Transaction =
            deserialize(&serialized_tx).context("failed to parse serialized tx")?;
        if txid != &tx.txid() {
            // This can happen on nexad, where `getrawtransaction` RPC call supports txidem also.
            bail!(
                "Requested transaction with txid {}, but received one with txid {}",
                txid,
                tx.txid()
            );
        }
        self.tx_cache.put(txid, serialized_tx);
        Ok(tx)
    }

    /// Returns the height the transaction is confirmed at.
    ///
    /// If the transaction is in mempool, it return -1 if it has unconfirmed
    /// parents, or 0 if not.
    ///
    /// Returns None if transaction does not exist.
    pub fn get_confirmation_height(&self, txid: Txid) -> Option<i64> {
        {
            match self.mempool.tx_confirmation_state(&txid, None) {
                ConfirmationState::InMempool => return Some(0),
                ConfirmationState::UnconfirmedParent => return Some(-1),
                _ => (),
            };
        }
        self.header
            .get_confirmed_height_for_tx(txid)
            .map(|height| height as i64)
    }

    /// Create input amount cache out of outsputs from given tx set, where possible.
    pub fn create_input_cache(&self, txids: &HashSet<Txid>) -> HashMap<OutPointHash, u64> {
        txids
            .iter()
            .filter_map(|txid| match self.get_unconfirmed(txid) {
                Ok(tx) => Some(
                    tx.output
                        .iter()
                        .enumerate()
                        .map(|(n, o)| {
                            let outpoint = compute_outpoint_hash_from_tx(&tx, n as u32);
                            (outpoint, o.value)
                        })
                        .collect::<Vec<(OutPointHash, u64)>>(),
                ),
                Err(_) => None,
            })
            .flatten()
            .collect::<HashMap<OutPointHash, u64>>()
    }

    /**
     * Create a mempool entry for given transaction ID.
     *
     * Note: We need to pass mempool, rather than use self.mempool to avoid dead lock,
     * since we call this method from the mempool itself.
     */
    pub fn get_mempool_entry(
        &self,
        mempool: &Tracker,
        txid: &Txid,
        input_cache: &HashMap<OutPointHash, u64>,
    ) -> Result<MempoolEntry> {
        let tx = self.get_unconfirmed(txid)?;

        let total_stats_in: u64 = tx
            .input
            .iter()
            .map(|i| {
                let outpoint = outpoint_hash(&i.previous_output);
                match input_cache.get(&outpoint) {
                    Some(sats) => Ok(*sats),
                    None => sats_from_outpoint(&mempool.index(), &outpoint)
                        .or_else(|| sats_from_outpoint(&self.confirmed_index, &outpoint))
                        .context(format!("Failed to get input {}", outpoint.to_hex())),
                }
            })
            .collect::<Result<Vec<u64>>>()?
            .into_iter()
            .sum();

        let total_sats_out: u64 = tx.output.iter().map(|o| o.value).sum();

        let fee: u64 = if total_sats_out > total_stats_in {
            if tx.is_coin_base() {
                0
            } else {
                // Invalid tx
                return Err(anyhow!("Inputs are greater than outputs"));
            }
        } else {
            total_stats_in - total_sats_out
        };

        Ok(MempoolEntry::new(fee, tx))
    }
}

#[cfg(test)]
mod test {

    // Test case for bug where output of tx on the nexa chain was encoded with an extra prefixed byte.
    // Issue #188
    #[cfg(feature = "nexa")]
    #[test]
    fn test_encode_address_bug() {
        use super::get_addresses;
        use crate::nexa;
        use bitcoincash::{consensus::deserialize, Network};

        let txhex = "00010090c4c1de7a046ed38738c852d11c3520912bde69139f5774faa394bcf244b0b664222103b9521b653db81b3e5dde95092fa8483e22deb7b9646bd3c68e90ff1364a66120403ffee795329706d8e1fe88393869088b7c421604fa1dce99b478d28d6b2e6f89162b2921b3c82f4ac0eb6ff2b49a083c679ef20e572ba2b78954297efdfc17d6feffffff85ad0a00000000000201e09304000000000017005114247a304a71f7c08a66df3d41eb25c1609b4d75f001ca1806000000000017005114264164083c381bb50ea386d0913d1b6b8597c3cc861d0300";
        let tx: nexa::transaction::Transaction =
            deserialize(&hex::decode(&txhex).unwrap()).unwrap();
        let addresses = get_addresses(&tx.output[1], Network::Bitcoin);

        assert_eq!(1, addresses.len());
        let (payload, _, _) = nexa::cashaddr::decode(&addresses[0]).unwrap();
        assert_eq!(payload, tx.output[1].script_pubkey.to_bytes());
        assert_eq!(
            "nexa:nqtsq5g5yeqkgzpu8qdm2r4rsmgfz0gmdwze0s7vuwn0psk9",
            addresses[0]
        );
    }
}
