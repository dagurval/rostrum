use crate::chaindef::OutPointHash;
use crate::chaindef::ScriptHash;
use crate::chaindef::TokenID;
use crate::chaindef::Transaction;
use crate::indexes::heightindex::HeightIndexRow;
use crate::indexes::inputindex::InputIndexRow;
use crate::indexes::outputindex::OutputIndexRow;
use crate::indexes::outputtokenindex::OutputTokenIndexRow;
use crate::indexes::scripthashindex::QueryFilter;
use crate::indexes::scripthashindex::ScriptHashIndexRow;
use crate::indexes::DBRow;
use crate::mempool::MEMPOOL_HEIGHT;
use crate::query::tx::TxQuery;
use crate::store::{DBStore, Row};
use anyhow::Result;
use bitcoin_hashes::Hash;
use bitcoincash::hash_types::Txid;

pub(crate) fn get_row<T: DBRow>(store: &DBStore, key: Vec<u8>) -> Option<T> {
    let row = store.get(T::CF, &key).map(|value| Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    });
    row.map(|r| T::from_row(&r))
}

/**
 * Fetche a single row found by key prefix.
 */
pub(crate) fn get_row_by_key_prefix<T: DBRow>(store: &DBStore, key: Vec<u8>) -> Option<T> {
    store.scan(T::CF, key).next().map(|r| T::from_row(&r))
}

/**
 * Find transaction height given a txid
 */
pub(crate) fn height_by_txid(store: &DBStore, txid: Txid) -> Option<u32> {
    let inner = txid.into_inner();
    let key = inner.as_slice();
    let value = store.get(HeightIndexRow::CF, key)?;
    let height = HeightIndexRow::height_from_dbvalue(&value);
    if height == MEMPOOL_HEIGHT {
        None
    } else {
        Some(height)
    }
}

/**
 * Collect all utxos sent to a scripthash.
 */
pub(crate) fn outputs_by_scripthash<'a>(
    store: &'a DBStore,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Box<dyn Iterator<Item = OutputIndexRow> + 'a> {
    let filter = ScriptHashIndexRow::filter_by_scripthash(scripthash.into_inner(), filter);

    Box::new(
        store
            .scan(ScriptHashIndexRow::CF, filter)
            .flat_map(move |scripthash: Row| {
                let scripthash = ScriptHashIndexRow::from_row(&scripthash);
                store
                    .scan(
                        OutputIndexRow::CF,
                        OutputIndexRow::filter_by_outpointhash(&scripthash.outpointhash()),
                    )
                    .map(|r| OutputIndexRow::from_row(&r))
            }),
    )
}

pub(crate) fn get_txheight_by_id(store: &DBStore, txid: &Txid) -> Option<HeightIndexRow> {
    let key = HeightIndexRow::filter_by_txid(txid);
    let value = store.get(HeightIndexRow::CF, &key)?;
    Some(HeightIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    }))
}

/**
 * If token_id is provided, filters result by token ID.
 */
pub(crate) fn txheight_by_scripthash<'a>(
    store: &'a DBStore,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Box<dyn Iterator<Item = HeightIndexRow> + 'a> {
    Box::new(
        outputs_by_scripthash(store, scripthash, filter)
            .map(|output| output.txid())
            .map(move |txid| {
                get_txheight_by_id(store, &txid)
                    .unwrap_or_else(|| panic!("expected tx {} to have a height", txid))
            }),
    )
}

/**
 * Get a single output defined by its outpointhash HASH(txid, n)
 */
pub(crate) fn get_utxo(store: &DBStore, outpointhash: &OutPointHash) -> Option<OutputIndexRow> {
    let key = outpointhash.to_vec();
    let value = store.get(OutputIndexRow::CF, &key)?;

    Some(OutputIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    }))
}

pub(crate) fn token_from_outpoint(
    store: &DBStore,
    outpointhash: &OutPointHash,
) -> Option<(TokenID, i64)> {
    let key = OutputTokenIndexRow::filter_by_outpointhash(outpointhash);
    match get_row_by_key_prefix::<OutputTokenIndexRow>(store, key) {
        Some(token_row) => {
            let amount = token_row.token_amount();
            Some((token_row.into_token_id(), amount))
        }
        None => None,
    }
}

pub(crate) fn get_tx_spending_prevout(
    store: &DBStore,
    txquery: &TxQuery,
    outpoint: &OutPointHash,
) -> Result<
    Option<(
        Transaction,
        u32, /* input index */
        u32, /* confirmation height */
    )>,
> {
    let spender = tx_spending_outpoint(store, outpoint);
    if spender.is_none() {
        return Ok(None);
    };
    let spender = spender.unwrap();
    let txid = spender.txid();
    let height = height_by_txid(store, txid);
    let tx = txquery.get(&txid, None, height)?;
    Ok(Some((tx, spender.index(), height.unwrap_or_default())))
}

pub(crate) fn output_is_spent(store: &DBStore, outpoint: OutPointHash) -> bool {
    store.exists(InputIndexRow::CF, outpoint.into_inner().as_slice())
}

// TODO: Rename to input_spending_output
pub(crate) fn tx_spending_outpoint(
    store: &DBStore,
    outpoint: &OutPointHash,
) -> Option<InputIndexRow> {
    let key = InputIndexRow::filter_by_outpointhash(outpoint);
    let value = store.get(InputIndexRow::CF, &key)?;
    Some(InputIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    }))
}

pub fn get_tx_funding_prevout(
    store: &DBStore,
    txquery: &TxQuery,
    outpoint: &OutPointHash,
) -> Result<
    Option<(
        Transaction,
        u32, /* input index */
        u32, /* confirmation height */
    )>,
> {
    let spender = tx_funding_outpoint(store, outpoint);
    if spender.is_none() {
        return Ok(None);
    };
    let spender = spender.unwrap();
    let txid = spender.txid();
    let height = height_by_txid(store, txid);
    let tx = txquery.get(&txid, None, height)?;
    Ok(Some((tx, spender.index(), height.unwrap_or_default())))
}

/**
 * Locate transaction that funded given output (created this utxo)
 */
pub fn tx_funding_outpoint(store: &DBStore, outpoint: &OutPointHash) -> Option<OutputIndexRow> {
    let key = OutputIndexRow::filter_by_outpointhash(outpoint);
    let value = store.get(OutputIndexRow::CF, &key)?;
    Some(OutputIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    }))
}
