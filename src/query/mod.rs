use crate::chain::Chain;
use crate::chaindef::Block;
use crate::chaindef::BlockHeader;
use crate::chaindef::OutPointHash;
use crate::discovery::announcer::PeerAnnouncer;
use crate::discovery::discoverer::PeerDiscoverer;
use crate::index::get_last_indexed_block;
use crate::indexes::scripthashindex::QueryFilter;
use crate::store::DBStore;
use rayon::prelude::*;

use crate::chaindef::ScriptHash;
use crate::chaindef::Transaction;
use crate::metrics;
use bitcoincash::consensus::encode::serialize;
use bitcoincash::hash_types::{TxMerkleNode, Txid};
use bitcoincash::hashes::hex::ToHex;
use bitcoincash::hashes::sha256d::Hash as Sha256dHash;
use bitcoincash::hashes::Hash;
use bitcoincash::network::constants::Network;
use serde_json::Value;
use std::collections::HashSet;
use std::sync::Arc;

use crate::app::App;
use crate::cache::TransactionCache;
use crate::cashaccount::{txids_by_cashaccount, CashAccountParser};
use crate::chaindef::BlockHash;
use crate::mempool::Tracker;
use crate::metrics::Metrics;
#[cfg(feature = "nexa")]
use crate::nexa::hash_types::TxIdem;
use crate::query::header::HeaderQuery;
use crate::query::queryutil::txheight_by_scripthash;
use crate::query::tx::TxQuery;
use crate::timeout::TimeoutTrigger;
use anyhow::{Context, Result};

use self::queryutil::get_tx_funding_prevout;
use self::queryutil::get_tx_spending_prevout;

pub mod header;
pub mod queryutil;
pub mod status;
pub mod token;
pub mod tx;

fn merklize<T: Hash>(left: T, right: T) -> T {
    let data = [&left[..], &right[..]].concat();
    <T as Hash>::hash(&data)
}

fn create_merkle_branch_and_root<T: Hash>(mut hashes: Vec<T>, mut index: usize) -> (Vec<T>, T) {
    let mut merkle = vec![];
    while hashes.len() > 1 {
        if hashes.len() % 2 != 0 {
            let last = *hashes.last().unwrap();
            hashes.push(last);
        }
        index = if index % 2 == 0 { index + 1 } else { index - 1 };
        merkle.push(hashes[index]);
        index /= 2;
        hashes = hashes
            .chunks(2)
            .map(|pair| merklize(pair[0], pair[1]))
            .collect()
    }
    (merkle, hashes[0])
}

pub struct Query {
    app: Arc<App>,
    tracker: Arc<Tracker>,
    duration: Arc<prometheus::HistogramVec>,
    tx: TxQuery,
    header: Arc<HeaderQuery>,
}

impl Query {
    pub fn new(
        app: Arc<App>,
        metrics: &Metrics,
        tx_cache: TransactionCache,
        tracker: Arc<Tracker>,
        network: Network,
    ) -> Result<Arc<Query>> {
        let daemon = app.daemon().reconnect()?;
        let duration = Arc::new(metrics.histogram_vec(
            "rostrum_query_duration",
            "Request duration (in seconds)",
            &["type"],
            metrics::default_duration_buckets(),
        ));
        let header = Arc::new(HeaderQuery::new(app.clone()));
        let tx = TxQuery::new(
            tx_cache,
            daemon,
            tracker.clone(),
            header.clone(),
            duration.clone(),
            network,
            app.index().read_store_clone(),
        );
        Ok(Arc::new(Query {
            app,
            tracker,
            duration,
            tx,
            header,
        }))
    }

    pub(crate) fn announcer(&self) -> &PeerAnnouncer {
        self.app.announcer()
    }

    pub(crate) fn chain(&self) -> &Chain {
        self.app.index().chain()
    }

    pub(crate) fn discoverer(&self) -> &PeerDiscoverer {
        self.app.discoverer()
    }

    #[cfg(feature = "nexa")]
    fn get_block_height(&self, header: &BlockHeader) -> Option<u64> {
        Some(header.height())
    }

    #[cfg(not(feature = "nexa"))]
    fn get_block_height(&self, header: &BlockHeader) -> Option<u64> {
        self.chain().get_block_height(&header.block_hash())
    }

    pub fn get_confirmed_blockhash(&self, tx_hash: Txid) -> Result<Value> {
        let header = self.header.get_by_txid(tx_hash, None)?;
        if header.is_none() {
            bail!("tx {} is unconfirmed or does not exist", tx_hash);
        }
        let header = header.unwrap();
        let height = self
            .get_block_height(&header)
            .context(anyhow!("No height for block {}", header.block_hash()))?;
        Ok(json!({
            "block_hash": header.block_hash(),
            "block_height": height,
        }))
    }

    pub fn get_headers(&self, heights: &[usize]) -> Vec<BlockHeader> {
        let _timer = self
            .duration
            .with_label_values(&["get_headers"])
            .start_timer();
        let index = self.app.index();
        heights
            .iter()
            .filter_map(|height| index.chain().get_block_header(*height))
            .collect()
    }

    /**
     * Get the header at tip of chain.
     *
     * Note: This returns the last completely INDEXED header, not the
     * last known header.
     */
    pub(crate) fn get_best_header(&self) -> Result<(u64, BlockHeader)> {
        let last_indexed = match get_last_indexed_block(self.confirmed_index()) {
            Some(h) => h,
            None => self.app.index().chain().genesis_hash(),
        };
        let height = self
            .app
            .index()
            .chain()
            .get_block_height(&last_indexed)
            .context("Failed to get best header")?;
        let header = self
            .app
            .index()
            .chain()
            .get_block_header(height as usize)
            .context("Failed to get best header")?;
        Ok((height, header))
    }

    pub fn getblocktxids(&self, blockhash: &BlockHash) -> Result<Vec<Txid>> {
        self.app.daemon().getblocktxids(blockhash)
    }

    /**
     * Get a block by hash
     */
    pub fn get_block(&self, blockhash: &BlockHash) -> Result<Block> {
        self.app.daemon().getblock(blockhash)
    }

    pub fn get_merkle_proof(
        &self,
        tx_hash: &Txid,
        height: usize,
    ) -> Result<(Vec<TxMerkleNode>, usize)> {
        let header_entry = self
            .app
            .index()
            .chain()
            .get_block_header(height)
            .context(format!("missing block #{}", height))?;
        let txids = self
            .app
            .daemon()
            .getblocktxids(&header_entry.block_hash())?;
        let pos = txids
            .iter()
            .position(|txid| txid == tx_hash)
            .context(format!("missing txid {}", tx_hash))?;
        let tx_nodes: Vec<TxMerkleNode> = txids
            .into_iter()
            .map(|txid| TxMerkleNode::from_inner(txid.into_inner()))
            .collect();
        let (branch, _root) = create_merkle_branch_and_root(tx_nodes, pos);
        Ok((branch, pos))
    }

    pub fn get_header_merkle_proof(
        &self,
        height: usize,
        cp_height: usize,
    ) -> Result<(Vec<Sha256dHash>, Sha256dHash)> {
        if cp_height < height {
            bail!("cp_height #{} < height #{}", cp_height, height);
        }

        let best_height = self.app.index().chain().height();
        if best_height < (cp_height as u64) {
            bail!(
                "cp_height #{} above best block height #{}",
                cp_height,
                best_height
            );
        }

        let heights: Vec<usize> = (0..=cp_height).collect();
        let header_hashes: Vec<BlockHash> = self
            .get_headers(&heights)
            .into_par_iter()
            .map(|h| h.block_hash())
            .collect();
        let merkle_nodes: Vec<Sha256dHash> = header_hashes
            .par_iter()
            .map(|block_hash| Sha256dHash::from_inner(block_hash.into_inner()))
            .collect();
        assert_eq!(header_hashes.len(), heights.len());
        Ok(create_merkle_branch_and_root(merkle_nodes, height))
    }

    pub fn get_id_from_pos(
        &self,
        height: usize,
        tx_pos: usize,
        want_merkle: bool,
    ) -> Result<(Txid, Vec<TxMerkleNode>)> {
        let header_entry = self
            .app
            .index()
            .chain()
            .get_block_header(height)
            .context(format!("missing block #{}", height))?;

        let txids = self
            .app
            .daemon()
            .getblocktxids(&header_entry.block_hash())?;
        let txid = *txids.get(tx_pos).context(format!(
            "No tx in position #{} in block #{}",
            tx_pos, height
        ))?;

        let tx_nodes = txids
            .into_iter()
            .map(|txid| TxMerkleNode::from_inner(txid.into_inner()))
            .collect();

        let branch = if want_merkle {
            create_merkle_branch_and_root(tx_nodes, tx_pos).0
        } else {
            vec![]
        };
        Ok((txid, branch))
    }

    #[cfg(not(feature = "nexa"))]
    pub fn broadcast(&self, txn: &Transaction) -> Result<Txid> {
        self.app.daemon().broadcast(txn)
    }

    #[cfg(feature = "nexa")]
    pub fn broadcast(&self, txn: &Transaction) -> Result<TxIdem> {
        self.app.daemon().broadcast(txn)
    }

    pub fn update_mempool(&self) -> Result<HashSet<Txid>> {
        let _timer = self
            .duration
            .with_label_values(&["update_mempool"])
            .start_timer();
        self.tracker
            .update_from_daemon(self.app.daemon(), self.tx())
    }

    /// Returns [vsize, fee_rate] pairs (measured in vbytes and satoshis).
    pub fn get_fee_histogram(&self) -> Vec<(f32, u32)> {
        self.tracker.fee_histogram()
    }

    // Fee rate [BTC/kB] to be confirmed in `blocks` from now.
    pub fn estimate_fee(&self, blocks: usize) -> f64 {
        let mut total_vsize = 0u32;
        let mut last_fee_rate = 0.0;
        let blocks_in_vbytes = (blocks * 1_000_000) as u32; // assume ~1MB blocks
        for (fee_rate, vsize) in self.tracker.fee_histogram() {
            last_fee_rate = fee_rate;
            total_vsize += vsize;
            if total_vsize >= blocks_in_vbytes {
                break; // under-estimate the fee rate a bit
            }
        }
        (last_fee_rate as f64) * 1e-5 // [BTC/kB] = 10^5 [sat/B]
    }

    pub fn get_banner(&self) -> Result<String> {
        self.app.get_banner()
    }

    pub fn get_cashaccount_txs(
        &self,
        name: &str,
        height: u32,
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let cashaccount_txs = txids_by_cashaccount(self.app.index().read_store(), name, height);

        #[derive(Serialize, Deserialize, Debug)]
        struct AccountTx {
            tx: String,
            height: u32,
            blockhash: String,
        }
        let header = self
            .header()
            .at_height(height as usize)
            .context(format!("missing header at height {}", height))?;
        let blockhash = header.block_hash();
        let blockhash_hex = blockhash.to_hex();

        let mut result: Vec<AccountTx> = vec![];
        let parser = CashAccountParser::new(None);

        for txid in cashaccount_txs {
            let tx = self.tx.get(&txid, Some(&blockhash), None)?;

            // Filter on name in case of txid prefix collision
            if !parser.has_cashaccount(&tx, name) {
                continue;
            }

            result.push({
                AccountTx {
                    tx: hex::encode(serialize(&tx)),
                    height,
                    blockhash: blockhash_hex.clone(),
                }
            });
            timeout.check()?;
        }

        Ok(json!(result))
    }

    /// Find first outputs to scripthash
    pub fn scripthash_first_use(
        &self,
        scripthash: ScriptHash,
        filter: QueryFilter,
    ) -> Result<Option<(u32, Txid)>> {
        let get_tx = |store| {
            let txs = txheight_by_scripthash(store, scripthash, filter);
            let tx = txs.min_by(|a, b| a.height().cmp(&b.height()));

            if let Some(tx) = tx {
                Ok(Some((tx.height(), tx.txid())))
            } else {
                Ok(None)
            }
        };

        // Look at blockchain first
        if let Some(tx) = get_tx(self.app.index().read_store())? {
            return Ok(Some(tx));
        }

        // No match in the blockchain, try the mempool also.
        let mempool = self.tracker.index();
        get_tx(&mempool)
    }

    pub fn get_relayfee(&self) -> Result<f64> {
        self.app.daemon().get_relayfee()
    }

    pub fn tx(&self) -> &TxQuery {
        &self.tx
    }

    pub fn header(&self) -> &HeaderQuery {
        &self.header
    }

    pub fn get_tx_spending_prevout(
        &self,
        outpoint: &OutPointHash,
    ) -> Result<
        Option<(
            Transaction,
            u32, /* input index */
            u32, /* confirmation height */
        )>,
    > {
        {
            let mempool = self.tracker.index();
            let spent = get_tx_spending_prevout(&mempool, &self.tx, outpoint)?;
            if spent.is_some() {
                return Ok(spent);
            }
        }
        let store = self.app.index().read_store();
        get_tx_spending_prevout(store, self.tx(), outpoint)
    }

    /**
     * Get the transaction funding a output (a utxo).
     *
     * Returns the transaction itself, the output index and height it was confirmed in (0 for unconfirmed).
     */
    pub fn get_tx_funding_prevout(
        &self,
        prevout: &OutPointHash,
    ) -> Result<
        Option<(
            Transaction,
            u32, /* output index */
            u32, /* confirmation height */
        )>,
    > {
        {
            let mempool = self.tracker.index();
            let spent = get_tx_funding_prevout(&mempool, &self.tx, prevout)?;
            if spent.is_some() {
                return Ok(spent);
            }
        }

        get_tx_funding_prevout(self.app.index().read_store(), &self.tx, prevout)
    }

    pub fn confirmed_index(&self) -> &DBStore {
        self.app.index().read_store()
    }

    pub fn unconfirmed_index(&self) -> &Tracker {
        &self.tracker
    }
}
