/**
 * This is copied from the `bitcoincash` crate, where it is not exported.
 */
#[macro_export]
macro_rules! impl_nexa_consensus_encoding {
    ($thing:ident, $($field:ident),+) => (
        impl bitcoincash::consensus::Encodable for $thing {
            #[inline]
            fn consensus_encode<R: std::io::Write + ?Sized>(
                &self,
                r: &mut R,
            ) -> Result<usize, std::io::Error> {
                let mut len = 0;
                $(len += self.$field.consensus_encode(r)?;)+
                Ok(len)
            }
        }

        impl bitcoincash::consensus::Decodable for $thing {

            #[inline]
            fn consensus_decode_from_finite_reader<R: std::io::Read + ?Sized>(
                r: &mut R,
            ) -> Result<$thing, bitcoincash::consensus::encode::Error> {
                Ok($thing {
                    $($field: bitcoincash::consensus::Decodable::consensus_decode_from_finite_reader(r)?),+
                })
            }

            #[inline]
            fn consensus_decode<R: std::io::Read + ?Sized>(
                r: &mut R,
            ) -> Result<$thing, bitcoincash::consensus::encode::Error> {
                use std::io::Read as _;
                let mut r = r.take(bitcoincash::consensus::encode::MAX_VEC_SIZE as u64);
                Ok($thing {
                    $($field: bitcoincash::consensus::Decodable::consensus_decode(r.by_ref())?),+
                })
            }
        }
    );
}

/**
 * This is copied from the `bitcoincash` crate, where it is not exported.
 */
#[macro_export]
macro_rules! impl_nexa_hashencode {
    ($hashtype:ident) => {
        impl bitcoincash::consensus::Encodable for $hashtype {
            fn consensus_encode<W: std::io::Write + ?Sized>(
                &self,
                w: &mut W,
            ) -> Result<usize, std::io::Error> {
                self.0.consensus_encode(w)
            }
        }

        impl bitcoincash::consensus::Decodable for $hashtype {
            fn consensus_decode<R: std::io::Read + ?Sized>(
                r: &mut R,
            ) -> Result<Self, bitcoincash::consensus::encode::Error> {
                use bitcoincash::hashes::Hash;
                Ok(Self::from_inner(
                    <<$hashtype as bitcoincash::hashes::Hash>::Inner>::consensus_decode(r)?,
                ))
            }
        }
    };
}
