use crate::cashaccount::CashAccountParser;
use crate::chain::undo_block;
use crate::chain::Chain;
use crate::chain::NewHeader;
use crate::chaindef::Block;
use crate::chaindef::BlockHash;
use crate::chaindef::BlockHeader;
use crate::chaindef::ScriptHash;
use crate::chaindef::{Transaction, TxIn};
use crate::config::Config;
use crate::daemon::Daemon;
use crate::encode::compute_outpoint_hash;
use crate::indexes::headerindex::HeaderRow;
use crate::indexes::heightindex::HeightIndexRow;
use crate::indexes::inputindex::InputIndexRow;
use crate::indexes::outputindex::OutputIndexRow;
use crate::indexes::scripthashindex::OutputFlags;
use crate::indexes::scripthashindex::ScriptHashIndexRow;
use crate::indexes::DBRow;
use crate::indexes::{
    outputtokenindex::OutputTokenIndexRow, tokenoutputindex::TokenOutputIndexRow,
};
use crate::metrics;
use crate::metrics::Metrics;
use crate::signal::Waiter;
use crate::store;
use crate::store::DBContents;
use crate::store::DBStore;
use crate::store::Row;
use crate::store::METADATA_LAST_INDEXED_BLOCK;
use crate::store::META_CF;
use crate::thread::spawn;
use crate::writebatch::WriteBatch;

use anyhow::Context;
use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use bitcoin_hashes::Hash;
use bitcoincash::consensus::encode::{deserialize, serialize};
use rayon::prelude::*;
use std::sync::Arc;
use std::sync::Mutex;
use std::time::Duration;
use std::time::Instant;

struct Stats {
    update_duration: prometheus::HistogramVec,
    blocks: prometheus::IntCounter,
    txns: prometheus::IntCounter,
    vsize: prometheus::IntCounter,
    height: prometheus::IntGauge,
}

impl Stats {
    fn new(metrics: &Metrics) -> Stats {
        Stats {
            update_duration: metrics.histogram_vec(
                "rostrum_update_duration",
                "Indexing update duration (in seconds)",
                &["step"],
                metrics::default_duration_buckets(),
            ),
            blocks: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_blocks",
                "# of indexed blocks",
            )),
            txns: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_txns",
                "# of indexed transactions",
            )),
            vsize: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_vsize",
                "# of indexed vbytes",
            )),
            height: metrics.gauge_int(prometheus::Opts::new(
                "rostrum_index_height",
                "Last indexed block's height",
            )),
        }
    }

    fn update(&self, block: &Block, height: usize) {
        self.blocks.inc();
        self.txns.inc_by(block.txdata.len() as u64);
        for tx in &block.txdata {
            self.vsize.inc_by(tx.size() as u64);
        }
        self.update_height(height);
    }

    fn update_height(&self, height: usize) {
        self.height.set(height as i64);
    }

    fn start_timer(&self, step: &str) -> prometheus::HistogramTimer {
        self.update_duration
            .with_label_values(&[step])
            .start_timer()
    }

    fn observe_chain(&self, chain: &Chain) {
        self.height.set(chain.height() as i64)
    }
}

#[cfg(not(feature = "nexa"))]
#[inline]
fn is_coinbase_input(input: &TxIn) -> bool {
    use bitcoincash::Txid;
    let null_hash: Txid = Txid::all_zeros();
    input.previous_output.txid == null_hash
}
#[cfg(feature = "nexa")]
#[inline]
fn is_coinbase_input(_input: &TxIn) -> bool {
    // nexa does not have inputs in coinbase
    false
}
#[cfg(not(feature = "nexa"))]
#[inline]
fn get_outpoint_hash(input: &TxIn) -> [u8; 32] {
    compute_outpoint_hash(&input.previous_output.txid, input.previous_output.vout).into_inner()
}
#[cfg(feature = "nexa")]
#[inline]
fn get_outpoint_hash(input: &TxIn) -> [u8; 32] {
    input.previous_output.hash.into_inner()
}

#[cfg(feature = "nexa")]
fn index_outputs(writebatch: &WriteBatch, tx: &Transaction) {
    let txid = tx.txid();
    let rows = tx.output.par_iter().enumerate().map(move |(i, output)| {
        OutputIndexRow::new(
            &txid,
            &compute_outpoint_hash(&tx.txidem(), i as u32),
            output.value,
            i as u32,
        )
        .to_row()
    });
    writebatch.insert(OutputIndexRow::CF, rows);
}

#[cfg(not(feature = "nexa"))]
fn index_outputs(writebatch: &WriteBatch, tx: &Transaction) {
    let txid = tx.txid();
    let rows = tx.output.par_iter().enumerate().map(move |(i, output)| {
        OutputIndexRow::new(
            &txid,
            &compute_outpoint_hash(&txid, i as u32),
            output.value,
            i as u32,
        )
        .to_row()
    });
    writebatch.insert(OutputIndexRow::CF, rows);
}

#[cfg(feature = "nexa")]
fn index_scriptsig(writebatch: &WriteBatch, tx: &Transaction) {
    let rows = tx
        .output
        .par_iter()
        .enumerate()
        .map(move |(index, output)| {
            {
                ScriptHashIndexRow::new(
                    &ScriptHash::normalized_from_txout(output),
                    &compute_outpoint_hash(&tx.txidem(), index as u32),
                    if output.has_token() {
                        OutputFlags::HasTokens
                    } else {
                        OutputFlags::None
                    },
                )
            }
            .to_row()
        });
    writebatch.insert(ScriptHashIndexRow::CF, rows);
}

#[cfg(not(feature = "nexa"))]
fn index_scriptsig(writebatch: &WriteBatch, tx: &Transaction) {
    let txid = tx.txid();
    let rows = tx
        .output
        .par_iter()
        .enumerate()
        .map(move |(index, output)| {
            {
                ScriptHashIndexRow::new(
                    &ScriptHash::from_script(&output.script_pubkey),
                    &compute_outpoint_hash(&txid, index as u32),
                    if output.has_token() {
                        OutputFlags::HasTokens
                    } else {
                        OutputFlags::None
                    },
                )
            }
            .to_row()
        });
    writebatch.insert(ScriptHashIndexRow::CF, rows);
}

#[cfg(feature = "nexa")]
fn index_token_outputs(writebatch: &WriteBatch, tx: &Transaction) {
    use crate::nexa::token::parse_token_from_scriptpubkey;
    tx.output.par_iter().enumerate().for_each(move |(i, out)| {
        if !out.has_token() {
            return;
        }
        if let Some((token, amount)) = parse_token_from_scriptpubkey(&out.script_pubkey) {
            let outpoint_hash = &compute_outpoint_hash(&tx.txidem(), i as u32);
            let out_token_row =
                OutputTokenIndexRow::new(outpoint_hash, token.clone(), amount).to_row();
            let token_out_row = TokenOutputIndexRow::new(token, outpoint_hash).to_row();
            writebatch.insert(OutputTokenIndexRow::CF, rayon::iter::once(out_token_row));
            writebatch.insert(TokenOutputIndexRow::CF, rayon::iter::once(token_out_row));
        };
    });
}

#[cfg(not(feature = "nexa"))]
fn index_token_outputs(writebatch: &WriteBatch, tx: &Transaction) {
    tx.output.par_iter().enumerate().for_each(move |(i, out)| {
        if let Some(t) = out.token.as_ref() {
            let outpoint_hash = compute_outpoint_hash(&tx.txid(), i as u32);
            let out_token_row = OutputTokenIndexRow::new(
                &outpoint_hash,
                t.id.clone(),
                t.commitment.clone(),
                t.amount,
            )
            .to_row();
            let token_out_row =
                TokenOutputIndexRow::new(t.id, t.commitment.clone(), &outpoint_hash).to_row();
            writebatch.insert(OutputTokenIndexRow::CF, rayon::iter::once(out_token_row));
            writebatch.insert(TokenOutputIndexRow::CF, rayon::iter::once(token_out_row));
        }
    });
}

pub fn index_transaction(
    writebatch: &WriteBatch,
    txn: &Transaction,
    height: usize,
    cashaccount: &Option<CashAccountParser>,
) {
    let txid = txn.txid();

    let inputs = txn
        .input
        .par_iter()
        .enumerate()
        .filter_map(move |(index, input)| {
            if is_coinbase_input(input) {
                None
            } else {
                let outpointhash = get_outpoint_hash(input);
                Some(InputIndexRow::new(outpointhash, txid, index as u32).to_row())
            }
        });
    writebatch.insert(InputIndexRow::CF, inputs);
    index_outputs(writebatch, txn);

    index_scriptsig(writebatch, txn);

    if let Some(c) = cashaccount {
        c.index_cashaccount(writebatch, txn, height as u32);
    };

    index_token_outputs(writebatch, txn);

    writebatch.insert(
        HeightIndexRow::CF,
        rayon::iter::once(HeightIndexRow::new(&txid, height as u32).to_row()),
    );
}

pub(crate) fn index_block<'a>(
    writebatch: &WriteBatch,
    block: &'a Block,
    height: usize,
    cashaccount: &'a Option<CashAccountParser>,
) {
    let blockhash = block.block_hash();
    let header_row = HeaderRow::new(&block.header).to_row();

    block
        .txdata
        .par_iter()
        .for_each(move |txn| index_transaction(writebatch, txn, height, cashaccount));
    writebatch.insert(HeaderRow::CF, rayon::iter::once(header_row));
    writebatch.insert(META_CF, rayon::iter::once(last_indexed_block(&blockhash)));
}

pub(crate) fn last_indexed_block(blockhash: &BlockHash) -> Row {
    // Store last indexed block (i.e. all previous blocks were indexed)
    Row {
        key: METADATA_LAST_INDEXED_BLOCK.to_vec().into_boxed_slice(),
        value: serialize(blockhash).into_boxed_slice(),
    }
}

pub(crate) fn get_last_indexed_block(store: &DBStore) -> Option<BlockHash> {
    assert!(store.contents == DBContents::ConfirmedIndex);
    if let Some(row) = store.get(META_CF, METADATA_LAST_INDEXED_BLOCK) {
        Some(deserialize(&row).expect("db error fetching blockhash"))
    } else {
        None
    }
}

fn read_indexed_headers(store: &DBStore) -> Vec<BlockHeader> {
    let (latest_blockhash, headers) = match store.get(META_CF, METADATA_LAST_INDEXED_BLOCK) {
        // latest blockheader persisted in the DB.
        Some(row) => {
            let tip: BlockHash = deserialize(&row).unwrap();
            let headers =
                HeaderRow::headers_up_to_tip(store, &tip).expect("Headers missing from database");
            (tip, headers)
        }
        None => (BlockHash::all_zeros(), vec![]),
    };
    info!("Latest indexed blockhash: {}", latest_blockhash.to_hex());

    assert_eq!(
        headers
            .first()
            .map(|h| h.prev_blockhash)
            .unwrap_or_else(BlockHash::all_zeros),
        BlockHash::all_zeros()
    );
    assert_eq!(
        headers
            .last()
            .map(BlockHeader::block_hash)
            .unwrap_or_else(BlockHash::all_zeros),
        latest_blockhash
    );
    headers
}

pub struct Index {
    store: Arc<DBStore>,
    daemon: Arc<Daemon>,
    stats: Stats,
    batch_size: usize,
    chain: Chain,
    cashaccount_activation_height: u32,
    low_memory: bool,
    db_write_cache_entries: usize,
    last_flush: Mutex<Instant>,
}

impl Index {
    pub fn load(
        store: Arc<DBStore>,
        mut chain: Chain,
        daemon: Arc<Daemon>,
        metrics: &Metrics,
        config: &Config,
    ) -> Result<Self> {
        let headers = read_indexed_headers(&store);
        if !headers.is_empty() {
            let tip = headers.last().unwrap().block_hash();
            chain.load(headers, tip);
            chain.drop_last_headers(
                &|hash, height| undo_block(&store, &daemon, hash, height),
                config.reindex_last_blocks as u64,
            );
            info!("Loaded {} headers from index", chain.height());
        }

        let stats = Stats::new(metrics);
        stats.observe_chain(&chain);
        Ok(Index {
            store,
            daemon,
            stats,
            batch_size: config.index_batch_size,
            chain,
            cashaccount_activation_height: config.cashaccount_activation_height,
            low_memory: config.low_memory,
            db_write_cache_entries: config.db_write_cache_entries,
            last_flush: Mutex::new(Instant::now()),
        })
    }

    /// Blockchain headers
    pub fn chain(&self) -> &Chain {
        &self.chain
    }

    pub fn update(&self) -> Result<(Vec<BlockHeader>, BlockHeader)> {
        let tip = self.daemon.getbestblockhash()?;
        let tip_height = self.daemon.getblockheader(&tip)?.1;
        let new_headers =
            metrics::observe_duration(&self.stats.update_duration, "new_headers", || {
                self.daemon.get_new_headers(&self.chain, &tip)
            })?;

        self.chain.update(
            &|hash, height| undo_block(&self.store, &self.daemon, hash, height),
            new_headers
                .iter()
                .map(|(h, height)| NewHeader::from_header_and_height(h.clone(), *height))
                .collect(),
            Some(tip_height),
        );

        let blockhashes: Vec<BlockHash> = new_headers.iter().map(|(h, _)| h.block_hash()).collect();

        let (block_sender, block_receiver) = crossbeam_channel::bounded(self.batch_size);
        let (write_sender, write_receiver) = crossbeam_channel::bounded(1);

        let daemon = self.daemon.clone();
        let block_fetcher = spawn("fetcher", move || {
            for blockhash in blockhashes.iter() {
                Waiter::shutdown_check()?;
                block_sender
                    .send(Some(daemon.getblock(blockhash)))
                    .context("failed sending blocks to be indexed")?;
            }
            block_sender
                .send(None)
                .context("failed sending explicit end of stream")?;
            Ok(())
        });

        let writer_store = Arc::clone(&self.store);
        let writer = spawn("index_writer", move || {
            loop {
                Waiter::shutdown_check()?;
                let batch: WriteBatch = match write_receiver.recv_timeout(Duration::from_secs(10)) {
                    Ok(Some(batch)) => batch,
                    Ok(None) =>
                    /* done */
                    {
                        break
                    }
                    Err(_) =>
                    /* recvtimeout */
                    {
                        continue
                    }
                };
                writer_store.write_batch(&batch);
            }
            Ok(())
        });

        let cashaccount = if self.cashaccount_activation_height > 0 {
            Some(CashAccountParser::new(Some(
                self.cashaccount_activation_height,
            )))
        } else {
            None
        };

        let mut writebatch = WriteBatch::new();

        loop {
            Waiter::shutdown_check()?;
            let timer = self.stats.start_timer("fetch");
            let block = match block_receiver.recv_timeout(Duration::from_secs(10)) {
                Ok(Some(block)) => block?,
                Ok(None) =>
                /* done */
                {
                    break
                }
                Err(_) =>
                /* timeout, poll waiter */
                {
                    continue
                }
            };

            timer.observe_duration();

            let blockhash = block.block_hash();
            let height = self
                .chain
                .get_block_height(&blockhash)
                .unwrap_or_else(|| panic!("missing header for block {}", blockhash));

            if height % 1000 == 0 && height != self.chain.height() {
                let chain_height = self.chain.height();
                info!(
                    "Indexing block {height} out of {chain_height}. Progress: {:.2}%",
                    (height as f64 / chain_height as f64) * 100.
                )
            }

            let timer = self.stats.start_timer("index");
            index_block(&writebatch, &block, height as usize, &cashaccount);
            timer.observe_duration();
            let timer = self.stats.start_timer("write");
            if self.low_memory || writebatch.len() >= self.db_write_cache_entries {
                if !self.low_memory {
                    debug!(
                        "Writing batch of {} entries to db at block {height}.",
                        self.db_write_cache_entries
                    );
                }
                write_sender
                    .send(Some(writebatch))
                    .context("failed to send batch for writing")?;
                writebatch = WriteBatch::new();
            }
            timer.observe_duration();
            self.stats.update(&block, height as usize);
            if self.last_flush.lock().unwrap().elapsed() > Duration::from_secs(10 * 60) {
                // Flush every 10 minutes to avoid having too much to reindex.
                *self.last_flush.lock().unwrap() = Instant::now();
                info!("Flushing index to disk...");
                self.store.flush()?;
                info!("Flushing done");
            }
        }

        write_sender
            .send(Some(writebatch))
            .context("failed to send batch for writing")?;
        write_sender
            .send(None)
            .context("failed to EOF to index writer")?;

        let timer = self.stats.start_timer("flush");
        timer.observe_duration();

        block_fetcher.join().expect("block fetcher failed");
        writer.join().expect("index writer failed");
        let tip_header = self.chain.tip();
        assert_eq!(&tip, &tip_header.block_hash());
        self.stats.observe_chain(&self.chain);
        Ok((
            new_headers.into_iter().map(|(h, _)| h).collect(),
            tip_header,
        ))
    }

    pub fn read_store(&self) -> &store::DBStore {
        &self.store
    }

    pub fn read_store_clone(&self) -> Arc<store::DBStore> {
        Arc::clone(&self.store)
    }
}
