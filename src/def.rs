pub const ROSTRUM_VERSION: &str = env!("CARGO_PKG_VERSION");
pub const PROTOCOL_VERSION_MIN: &str = "1.4";
pub const PROTOCOL_VERSION_MAX: &str = "1.4.3";
pub const PROTOCOL_HASH_FUNCTION: &str = "sha256";
// (Partial) DB history:
// 2.11: Changed prefix extractor to 32 bytes.
pub const DATABASE_VERSION: &str = "2.11";
#[cfg(not(feature = "nexa"))]
pub const COIN: u64 = 100_000_000;
#[cfg(feature = "nexa")]
pub const COIN: u64 = 100;
