#!/usr/bin/env python3
# Copyright (c) 2022 The Bitcoin Unlimited developers
import asyncio
from test_framework.util import assert_equal, wait_for_async
from test_framework.electrumutil import (
    ElectrumTestFramework,
    get_txid_from_idem,
)
from test_framework.environment import on_bch, on_nex
from test_framework.electrumconnection import ElectrumConnection

DUST = 546


class ElectrumTokenListUnspentTests(ElectrumTestFramework):
    def run_test(self):
        # This test users nexad wallet to create and send tokens.
        # Mine and mature some coins.
        n = self.nodes[0]
        n.generate(101)

        async def async_tests():
            cli = ElectrumConnection()
            await cli.connect()
            await self.sync_height(cli)
            try:
                await self.test_listunspent(n, cli)
            finally:
                cli.disconnect()

        asyncio.run(async_tests())

    async def test_listunspent(self, n, cli):
        addr = n.getnewaddress()
        addr_mint = n.getnewaddress()
        addr_scripthash = await cli.call("blockchain.address.get_scripthash", addr)
        utxo = (await cli.call("token.address.listunspent", addr))["unspent"]
        assert_equal(0, len(utxo))
        assert_equal(
            utxo,
            (await cli.call("token.scripthash.listunspent", addr_scripthash))[
                "unspent"
            ],
        )

        token1_id = self.create_token(to_addr=addr_mint, mint_amount=100)

        txidem = self.send_token(token1_id, addr, 42)
        txid = get_txid_from_idem(n, txidem)

        async def fetch_utxo():
            utxo = (await cli.call("token.address.listunspent", addr))["unspent"]
            if len(utxo) > 0:
                return utxo
            return None

        utxo = await wait_for_async(10, fetch_utxo)
        assert_equal(1, len(utxo))

        assert_equal(0, utxo[0]["height"])
        assert_equal(txid, utxo[0]["tx_hash"])
        if on_bch():
            # The 1000 amount is from test_framework.bch.utiltoken
            assert_equal(1000, utxo[0]["value"])
            assert "token_id" in utxo[0]
        elif on_nex():
            assert_equal(DUST, utxo[0]["value"])
            assert "token_id_hex" in utxo[0]
            assert_equal(token1_id, utxo[0]["group"])
        else:
            raise NotImplementedError()
        assert_equal(42, utxo[0]["token_amount"])
        assert utxo[0]["tx_pos"] in [0, 1]

        assert_equal(
            utxo,
            (await cli.call("token.scripthash.listunspent", addr_scripthash))[
                "unspent"
            ],
        )

        n.generate(1)

        async def wait_for_confheight():
            utxo = (await cli.call("token.address.listunspent", addr))["unspent"]
            return len(utxo) == 1 and utxo[0]["height"] == n.getblockcount()

        await wait_for_async(10, wait_for_confheight)


if __name__ == "__main__":
    ElectrumTokenListUnspentTests().main()
