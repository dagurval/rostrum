#!/usr/bin/env python3
# Copyright (c) 2022 The Bitcoin Unlimited developers
import asyncio
from test_framework.util import assert_equal
from test_framework.electrumutil import (
    ElectrumTestFramework,
)
from test_framework.electrumconnection import ElectrumConnection
from test_framework.environment import on_nex, on_bch


class ElectrumTokenGenesisInfo(ElectrumTestFramework):
    def run_test(self):
        # This test users nexad wallet to create and send tokens.
        # Mine and mature some coins.
        n = self.nodes[0]
        n.generate(101)

        async def async_tests():
            cli = ElectrumConnection()
            await cli.connect()
            self.sync_height(cli)
            try:
                await self.test_basic(n, cli)
                await self.test_with_token_history(n, cli)
                await self.test_decimal_places(n, cli)
            finally:
                cli.disconnect()

        asyncio.run(async_tests())

    async def test_basic(self, n, cli):
        addr = n.getnewaddress()

        if on_nex():
            ticker = "TICKER"
            name = "Some Name"
            url = "https://example.org"
            doc_hash = (
                "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
            )

            token_id = self.create_token(
                ticker, name, url, doc_hash, to_addr=addr, mint_amount=100
            )
        elif on_bch():
            token_id = self.create_token(to_addr=addr, mint_amount=100)
        else:
            raise NotImplementedError()

        # Get token info from mempool NYI for NEX
        n.generate(1)
        await self.sync_height(cli)

        info = await cli.call("token.genesis.info", token_id)
        assert "height" in info
        if on_nex():
            assert "txid" in info
            assert "txidem" in info
            assert_equal(token_id, info["group"])
            assert "token_id_hex" in info
            assert_equal(doc_hash, info["document_hash"])
            assert_equal(ticker, info["ticker"])
            assert_equal(name, info["name"])
            assert_equal(0, info["decimal_places"])
        if on_bch():
            assert "tx_hash" in info
            assert not "document_hash" in info
            assert not "ticker" in info
            assert not "name" in info

    async def test_with_token_history(self, n, cli):
        """
        Check that electrum is able to find genesis also when there are
        other token transactions within the same block as genesis transaction
        """
        if on_bch():
            # NYI - skip
            return

        addr = n.getnewaddress()

        token_id = self.create_token(
            "DUMMY", "dummy token", to_addr=addr, mint_amount=20
        )

        for _ in range(1, 20):
            n.token("send", token_id, addr, 10)

        n.generate(1)
        await self.sync_height(cli)
        info = await cli.call("token.genesis.info", token_id)
        assert_equal("DUMMY", info["ticker"])

    async def test_decimal_places(self, n, cli):
        if on_bch():
            # Not supported - skip
            return

        ticker = "TICKER"
        name = "Some Name"
        url = "https://example.org"
        doc_hash = "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
        decimal_places = 11

        addr = n.getnewaddress()
        token_id_with_decimals = self.create_token(
            ticker, name, url, doc_hash, decimal_places, to_addr=addr, mint_amount=20
        )

        token_id_no_decimal = self.create_token(
            ticker, name, url, doc_hash, to_addr=addr, mint_amount=20
        )
        n.generate(1)
        await self.sync_height(cli)

        info = await cli.call("token.genesis.info", token_id_with_decimals)
        assert_equal(11, info["decimal_places"])
        info = await cli.call("token.genesis.info", token_id_no_decimal)
        assert_equal(0, info["decimal_places"])


if __name__ == "__main__":
    ElectrumTokenGenesisInfo().main()
