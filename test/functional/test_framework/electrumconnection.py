import time
from test_framework.portseed import electrum_rpc_port
from test_framework.connectrum.client import StratumClient
from test_framework.connectrum.svr_info import ServerInfo


class TestClient(StratumClient):
    is_connected = False

    def connection_lost(self, protocol):
        self.is_connected = False
        super().connection_lost(protocol)


class ElectrumConnection:
    def __init__(self, loop=None, *, warn_on_connection_loss=False):
        self.cli = TestClient(loop, warn_on_connection_loss)

    async def connect(self, node_index=0):
        connect_timeout = 30

        start = time.time()

        while True:
            try:
                await self.cli.connect(
                    ServerInfo(
                        None, ip_addr="127.0.0.1", ports=electrum_rpc_port(n=node_index)
                    )
                )
                self.cli.is_connected = True
                break

            except Exception as e:
                if time.time() >= (start + connect_timeout):
                    raise Exception(
                        f"Failed to connect to electrum server. Error '{e}'"
                    ) from e

            time.sleep(1)

    def disconnect(self):
        self.cli.close()

    async def call(self, method, *args):
        if not self.cli.is_connected:
            raise Exception("not connected")
        return await self.cli.RPC(method, *args)

    async def subscribe(self, method, *args):
        if not self.cli.is_connected:
            raise Exception("not connected")
        future, queue = self.cli.subscribe(method, *args)
        result = await future
        return result, queue

    def is_connected(self):
        return self.cli.is_connected
