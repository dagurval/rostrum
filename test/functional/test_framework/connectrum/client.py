#
# Client connect to an Electrum server.
#

# Runtime check for optional modules
from collections import defaultdict
import traceback
import asyncio
import ssl
import logging

from .protocol import StratumProtocol
from .exc import ElectrumErrorResponse

logger = logging.getLogger(__name__)


# pylint: disable=too-many-instance-attributes
class StratumClient:
    def __init__(self, loop=None, warn_on_connection_loss=True):
        self.warn_on_connection_loss = warn_on_connection_loss
        """
            Setup state needed to handle req/resp from a single Stratum server.
            Requires a transport (TransportABC) object to do the communication.
        """
        self.protocol = None

        self.next_id = 1
        self.inflight = {}
        self.subscriptions = defaultdict(list)

        self.actual_connection = {}

        self.ka_task = None

        self.loop = loop or asyncio.get_event_loop()

        self.reconnect = None  # call connect() first

        self.server_info = None
        self.proto_code = None

        # next step: call connect()

    def connection_lost(self, protocol):
        # Ignore connection_lost for old connections
        if protocol is not self.protocol:
            return

        self.protocol = None
        if self.warn_on_connection_loss:
            logger.warning(
                "Electrum server connection lost. Traceback: %s", traceback.format_exc()
            )

        # cleanup keep alive task
        if self.ka_task:
            self.ka_task.cancel()
            self.ka_task = None

    def close(self):
        if self.protocol:
            self.protocol.close()
            self.protocol = None
        if self.ka_task:
            self.ka_task.cancel()
            self.ka_task = None

    async def connect(
        self,
        server_info,
        proto_code=None,
        *,
        use_tor=False,
        disable_cert_verify=False,
        proxy=None,
        short_term=False
    ):
        """
        Start connection process.
        Destination must be specified in a ServerInfo() record (first arg).
        """
        self.server_info = server_info
        if not proto_code:
            proto_code, *_ = server_info.protocols
        self.proto_code = proto_code

        logger.debug("Connecting to: %r", server_info)

        if proto_code == "g":  # websocket
            # to do this, we'll need a websockets implementation that
            # operates more like a asyncio.Transport
            # maybe: `asyncws` or `aiohttp`
            raise NotImplementedError("sorry no WebSocket transport yet")

        hostname, port, use_ssl = server_info.get_port(proto_code)

        if use_tor:
            raise Exception("tor not supported")

        if use_ssl and disable_cert_verify:
            # Create a more liberal SSL context that won't
            # object to self-signed certicates. This is
            # very bad on public Internet, but probably ok
            # over Tor
            use_ssl = ssl.create_default_context()
            use_ssl.check_hostname = False
            use_ssl.verify_mode = ssl.CERT_NONE

            logger.debug(" .. SSL cert check disabled")

        async def _reconnect():
            if self.protocol:
                return  # race/duplicate work

            if proxy:
                raise Exception("proxy not supported")

            transport, protocol = await self.loop.create_connection(
                StratumProtocol, host=hostname, port=port, ssl=use_ssl
            )

            self.protocol = protocol
            protocol.client = self

            # capture actual values used
            self.actual_connection = {
                "hostname": hostname,
                "port": int(port),
                "ssl": bool(use_ssl),
                "tor": bool(proxy),
            }
            self.actual_connection["ip_addr"] = transport.get_extra_info(
                "peername", default=["unknown"]
            )[0]

            if not short_term:
                self.ka_task = self.loop.create_task(self._keepalive())

            logger.debug("Connected to: %r", server_info)

        # close whatever we had
        if self.protocol:
            logger.warning(
                "connect called when already connected, closing previous connection"
            )
            self.protocol.close()
            self.protocol = None

        self.reconnect = _reconnect
        await self.reconnect()

    async def _keepalive(self):
        """
        Keep our connect to server alive forever, with some
        pointless traffic.
        """
        while self.protocol:
            vers = await self.RPC("server.version")
            logger.debug("Server version: %s", repr(vers))

            # Five minutes isn't really enough anymore; looks like
            # servers are killing 2-minute old idle connections now.
            # But decreasing interval this seems rude.
            await asyncio.sleep(600)

    def _send_request(self, method, params=None, is_subscribe=False):
        """
        Send a new request to the server. Serialized the JSON and
        tracks id numbers and optional callbacks.
        """
        if params is None:
            params = []
        # pick a new ID
        self.next_id += 1
        req_id = self.next_id

        # serialize as JSON
        msg = {"id": req_id, "method": method, "params": params}

        # subscriptions are a Q, normal requests are a future
        if is_subscribe:
            wait_q = asyncio.Queue()
            self.subscriptions[method].append(wait_q)

        fut = asyncio.Future(loop=self.loop)

        self.inflight[req_id] = (msg, fut)

        # send it via the transport, which serializes it
        if not self.protocol:
            logger.debug("Need to reconnect to server")

            async def connect_first():
                await self.reconnect()
                self.protocol.send_data(msg)

            self.loop.create_task(connect_first())
        else:
            # typical case, send request immediatedly, response is a future
            self.protocol.send_data(msg)

        return fut if not is_subscribe else (fut, wait_q)

    def _got_response(self, msg):
        """
        Decode and dispatch responses from the server.

        Has already been unframed and deserialized into an object.
        """

        # logger.debug("MSG: %r" % msg)

        resp_id = msg.get("id", None)

        if resp_id is None:
            # subscription traffic comes with method set, but no req id.
            method = msg.get("method", None)
            if not method:
                logger.error(
                    "Incoming server message had no ID nor method in it %s", msg
                )
                return

            # not obvious, but result is on params, not result, for
            # subscriptions
            result = msg.get("params", None)

            logger.debug("Traffic on subscription: %s", method)

            subs = self.subscriptions.get(method)
            for queue in subs:
                self.loop.create_task(queue.put(result))

            return

        assert "method" not in msg
        result = msg.get("result")

        # fetch and forget about the request
        inf = self.inflight.pop(resp_id)
        if not inf:
            logger.error("Incoming server message had unknown ID in it: %s", resp_id)
            return

        # it's a future which is done now
        req, retval = inf

        if "error" in msg:
            err = msg["error"]

            logger.info("Error response: '%s'", err)
            retval.set_exception(ElectrumErrorResponse(err, req))

        else:
            retval.set_result(result)

    # pylint: disable=invalid-name
    def RPC(self, method, *params):
        """
        Perform a remote command.

        Expects a method name, which look like:

            blockchain.address.get_balance

        .. and sometimes take arguments, all of which are positional.

        Returns a future which will you should await for
        the result from the server. Failures are returned as exceptions.
        """
        assert "." in method
        # assert not method.endswith('subscribe')
        return self._send_request(method, params)

    def subscribe(self, method, *params):
        """
        Perform a remote command which will stream events/data to us.

        Expects a method name, which look like:
            server.peers.subscribe
        .. and sometimes take arguments, all of which are positional.

        Returns a tuple: (Future, asyncio.Queue).
        The future will have the result of the initial
        call, and the queue will receive additional
        responses as they happen.
        """
        assert "." in method
        assert method.endswith("subscribe")
        return self._send_request(method, params, is_subscribe=True)
