## blockchain.utxo.get

Returns data on a specified output of specific transaction. Returns error
if transaction or output does not exist.

If the output is spent, information about the spender is provided. This allows
a SPV client to call `blockchain.transaction.get\_merkle` to generate a merkle
branch, proving that it is spent.

=== "Bitcoin Cash"

    **Signature**

    >  Function: `blockchain.utxo.get(tx_hash, output_index)`
    >
    >  Version added: Rostrum 7.0
    >
    >  Update Rostrum 8.1: Add token details and scriptpukey
    >
    > *tx_hash*
    >
    > > The transaction ID as hexadecimal string.
    >
    > *output\_index*
    >
    > > The vout position in the transaction.

    **Result**

    > A dictionary with the following keys:
    >
    > -  *status*
    >
    >    State of the utxo. A string that is "spent" or "unspent".
    >
    > -  *height*
    >
    >    The height the utxo was confirmed in. If it is unconfirmed, the
    >    value is 0 if all inputs are confirmed, and -1 otherwise.
    >
    > -  *value*
    >
    >    The output’s value in minimum coin units (satoshis).
    >
    > -  *scripthash*
    >
    >    The scriphash of the output scriptPubKey.
    >
    > -  *scriptpubkey*
    >
    >    The locking script of the utxo as hex.
    >
    > -  *spent*
    >
    >    The transaction spending the utxo with the following keys:
    >
    > >  -  *tx\_pos*
    > >
    > >    The zero-based index of the input in the transaction’s list of inputs. Null if utxo is unspent.
    > >
    > > -  *tx_hash*
    > >
    > >    The transaction ID. Null if utxo is unspent.
    > >
    > > -  *height*
    > >
    > >    The height the transaction was confirmed in. If it is unconfirmed, the
    > >    value is 0 if all inputs are confirmed, and -1 otherwise. Null if utxo is unspent.
    >
    > -  *spent*
    >
    >    The transaction spending the utxo with the following keys:
    >
    > -  *token_id*
    >
    >    The token id as hex. Null if none.
    >
    > -  *token_amount*
    >
    >    The amount of token in utxo. Null if none.
    >
    > -  *commitment*
    >
    >    The token commitment. Null if none.
    >
    > -  *token_bitfield*
    >
    >    The token bitfield. Null if none.

    **Example Results**
    ```
    {
        "amount": 1000,
        "commitment": "",
        "height": -1,
        "scripthash": "e35310a0f7115f2f24324456a40321dc2cdf91f6589f9d255ec3a7d450236d68",
        "scriptpubkey": "76a914a3cc10aa12e556fa0e77742c723436d2f16fe9ec88ac",
        "spent": {
            "height": null,
            "tx_hash": null,
            "tx_pos": null
        },
        "status": "unspent",
        "token_amount": 42,
        "token_bitfield": 16,
        "token_id": "e9a0b8cbf8aa5ae3eeb32528102839c28afa4d9d49523e01df4b98166bb0b040"
    }

    ```

=== "Nexa"

    >
    >  Function: `blockchain.utxo.get(outpoint_hash)`
    >
    >  Version added: Rostrum 7.0
    >
    >  Update Rostrum 8.1: Add group details and scriptpukey
    >
    > *outpoint\_hash*
    >
    > > Hash of utxo (hash of transaction idem + output index)

    **Result**

    > A dictionary with the following keys:
    >
    > -  *status*
    >
    >    State of the utxo. A string that is "spent" or "unspent".
    >
    > -  *height*
    >
    >    The height the utxo was confirmed in. If it is unconfirmed, the
    >    value is 0 if all inputs are confirmed, and -1 otherwise.
    >
    > -  *value*
    >
    >    The output’s value in minimum coin units (satoshis).
    >
    > -  *scripthash*
    >
    >    The scriphash of the output scriptPubKey.
    >
    > -  *scriptpubkey*
    >
    >    The locking script of the utxo as hex.
    >
    > -  *spent*
    >
    >    The transaction spending the utxo with the following keys:
    >
    > >  -  *tx\_pos*
    > >
    > >    The zero-based index of the input in the transaction’s list of inputs. Null if utxo is unspent.
    > >
    > > -  *tx_hash*
    > >
    > >    The transaction ID. Null if utxo is unspent.
    > >
    > > -  *height*
    > >
    > >    The height the transaction was confirmed in. If it is unconfirmed, the
    > >    value is 0 if all inputs are confirmed, and -1 otherwise. Null if utxo is unspent.
    >
    > - *tx_idem*
    >
    >  The transaction idem of the utxo.
    >
    > - *tx_pos*
    >
    > The output index of the utxo.
    >
    > - *group*
    >
    > The group ID as cashaddr. Null if none.
    >
    > - *token_id_hex*
    >
    > The group ID as hex. Null if none.
    >
    > - *group_quantity*
    >
    > The amount of group in utxo. Null if none.
    >
    > - *template_scripthash*
    >
    > The template scripthash. Null if not template type utxo.
    >
    > - *template_argumenthash*
    >
    > The template argumenthash. Null if not template type utxo.

    **Example Results**
    ```
    {
        "amount": 546,
        "group": "nexareg:tqqltqwqg3pjmu3fx4cs6y80g82zv7ag0a2hy5fnh3ada9twa5qqq20mluan6",
        "group_authority": 0,
        "group_quantity": 42,
        "height": -1,
        "scripthash": "d1e77faf0f8926a66eae9cfd15b1e85865c9efb72b3227328cb41e26d3d893a9",
        "scriptpubkey": "2001f581c044432df22935710d10ef41d4267ba87f55725133bc7ade956eed0000022a005114ac66342932657b6b0d1d64e8d7f80327890a8ea9",
        "spent": {
            "height": null,
            "tx_hash": null,
            "tx_pos": null
        },
        "status": "unspent",
        "template_argumenthash": "ac66342932657b6b0d1d64e8d7f80327890a8ea9",
        "template_scripthash": "pay2pubkeytemplate",
        "token_id_hex": "01f581c044432df22935710d10ef41d4267ba87f55725133bc7ade956eed0000",
        "tx_hash": "4c560f76d1e30c63b095a01d006942b73ef88652cf3055c0b04dbd529bec67ba",
        "tx_idem": "35a24f03b2b3d924fcd376b883419076f37313cd13a3dd479f7a34e87acac055",
        "tx_pos": 0
    }
    ```
