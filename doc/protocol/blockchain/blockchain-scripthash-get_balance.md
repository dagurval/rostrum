## blockchain.scripthash.get\_balance

Return the confirmed and unconfirmed balances of a `script hash`.

**Signature**

> Function:
> blockchain.scripthash.get\_balance(scripthash, filter="include\_tokens")
>
> Version added:
> 1.1
>
> -   *scripthash*
>
>     The script hash as a hexadecimal string.
>
> -   *filter*
>
>     Filter what utxos are included in the query. Valid filters are:
>
>     - include\_tokens - Include all utxos
>     - tokens\_only - Only include token utxos
>     - exclude\_token - Only include utxos without tokens

**Result**

> A dictionary with keys `confirmed` and `unconfirmed`. The value of each is the appropriate
> balance in satoshis.

**Result Example**

    {
      "confirmed": 103873966,
      "unconfirmed": 236844
    }
