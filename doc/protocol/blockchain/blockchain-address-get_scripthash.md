## blockchain.address.get\_scripthash

Translate a Bitcoin Cash or a Nexa address to a [script hash](/protocol/basics.md). This method is
potentially useful for clients preferring to work with `script hashes` but
lacking the local libraries necessary to generate them.

**Signature**

> Function:
> blockchain.address.get\_scripthash(address)
>
> Version added:
> 1.4.3
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Legacy addresses (base58) are also supported.

**Result**

> The unique 32-byte hex-encoded `script hash` that corresponds to the decoded address.