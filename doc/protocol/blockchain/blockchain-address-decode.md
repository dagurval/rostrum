## blockchain.address.decode

Decode a Bitcoin Cash or a Nexa address to its raw payload. This method is
potentially useful for clients needing to see the encoded contents of a address but
lacking the local libraries necessary to decode them.

**Signature**

> Function:
> blockchain.address.decode(address)
>
> Version added:
> Rostrum 7.0
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Some server implementations may also support Legacy (base58)
>     addresses but are not required to do so by this specification.

**Result**

> The decoded payload of the address and the type of data it represents.

**Example Result**

    {
      "payload": "96cbbc84783888e4cc971ae8acf86dd3c1a41937",
      "type": "p2pkh",
      "is_token_aware": "false"
    }