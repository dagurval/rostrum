## cashaccount.query.name

BCH ONLY.

Requires that it has been enabled in rostrum with the `cashaccount_activation_height`
parameter.

Returns the transactions registering a cashaccount at blockheight. Note that
height is absolute blockheight and you need to add the cashaccount block
modification value yourself.

The cashaccount block modification value for Bitcoin Cash is 563620.

For example, to lookup dagur#216, call `blockchain.query.name("dagur", 216 +
563620)`

**Signature**

> Function BCH: `cashaccount.query.name(name, height)`
>
>  Version added: ROSTRUM ONLY. This function is a extension added to
>  rostrum and may not be available on other electrum server
>  implementations.
>
> *name*
>
> > Cash account name
>
> *height*
>
> > Block height for registration (without cashaccount offset subtracted)

**Result**

> A dictionary with the following keys:
>
> -  *blockhash*
>
>    Block hash of the block that the cash account was confirmed in.
>
> -  *height*
>
>    Block height of the block that the cash account was confirmed in.
>
> -  *tx*
>
>    A hex string of the transaction containing the cash account.

**Example Results**

For query `blockchain.query.name('dagur', 563836)`

```
{
    'blockhash': '000000000000000003c73e50b9de6317c4d2b2ac5f3c1253b01e61a6e329219a',
    'height': 563836,
    'tx': '0100000001bca903bbc429218234857628b382e8aa8e3bfa74c5b59628ad053284e50bf6ac010000006b4830450221009bbd0a96ef5ef33e09c4fce7fafd2add714ebe05d87a9cb6c826b863d0e99225022039d77b8bd9c8067636e64d6f1aeeeeb8b816bbc875afd04cef9eb299df83b7d64121037a291b1a7f21b03b2a5120434b7a06b61944e0edc1337c76d737d0b5fa1c871fffffffff020000000000000000226a040101010105646167757215018c092ec2cbd842e89432c7c53b54db3a958c83a575f00d00000000001976a914dfdd3e914d73fee85ad40cd71430327f0404c15488ac00000000'
}
```