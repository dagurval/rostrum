## token.address.listunspent

Return an list of token UTXOs sent to a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.listunspent(address, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

**Result**

> As for `token.scripthash.listunspent`