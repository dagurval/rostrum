## token.address.get\_balance

Return the confirmed and unconfirmed balances of tokens in a Bitcoin Cash or Nexa address.

**Signature**

> Function:
> token.address.get\_balance(address)
>
> Version added: Rostrum 6.0
>
> -   *address*
>
>     The address as a Nexa or Cash Address string (with or without prefix).

**Result**

> See `token.scripthash.get_balance`.